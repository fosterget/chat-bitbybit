using Azure.Storage.Blobs;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Repositories;
using Messanger.Core.Interfaces.Repositories.BlobStorage;
using Messanger.Infrastructure.Generators;
using Messanger.Infrastructure.Repositories;
using Messanger.Infrastructure.Repositories.BlobStorage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;

namespace Messanger.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddSingleton(x => 
            new BlobServiceClient(configuration.GetConnectionString("AzureBlobStorage")));
        
        services.AddSingleton<IConnectionMultiplexer>(opt => 
            ConnectionMultiplexer.Connect(configuration.GetConnectionString("Redis")));

        services.AddTransient<IProfileImageRepository, ProfileImageRepository>();
        services.AddTransient<IDialogImageRepository, DialogImageRepository>();
        
        services.AddTransient<IStepRepository, StepRepository>();
        services.AddTransient<IStepButtonRepository, StepButtonRepository>();
        services.AddTransient<IInterlocutorRepository, InterlocutorRepository>();
        services.AddTransient<ITeamRepository, TeamRepository>();
        services.AddTransient<IFileRepository, FileRepository>();

        services.AddTransient<IBotDataRepository, BotDataRepository>();
        services.AddTransient<IBotResultsCsvGenerator, BotResultsCsvGenerator>();
        
        return services;
    }
}