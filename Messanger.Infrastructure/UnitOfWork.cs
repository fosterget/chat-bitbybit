﻿using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Repositories;

namespace Messanger.Infrastructure;

public class UnitOfWork : IUnitOfWork
{
    private readonly ApplicationDbContext _context;
    public IDialogRepository Dialogs { get; }
    
    public IMessageRepository Messages { get; }
    
    public IInterlocutorRepository Interlocutors { get; }
    
    public IUserRepository Users { get; }
    
    public IBotRepository Bots { get; }
    
    public IStepRepository Steps { get; }
    
    public ITeamRepository Teams { get; }
    
    public IFileRepository Files { get; }

    public UnitOfWork(
        ApplicationDbContext context, 
        IDialogRepository dialogs, 
        IMessageRepository messages, 
        IUserRepository users, 
        IBotRepository bots, 
        IStepRepository steps,
        IInterlocutorRepository interlocutors,
        ITeamRepository teams,
        IFileRepository files)
    {
        _context = context;
        Dialogs = dialogs;
        Messages = messages;
        Users = users;
        Bots = bots;
        Steps = steps;
        Interlocutors = interlocutors;
        Teams = teams;
        Files = files;
    }
    
    public async Task<int> Complete()
    {
        return await _context.SaveChangesAsync();
    }
}