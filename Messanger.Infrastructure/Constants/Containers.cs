namespace Messanger.Infrastructure.Constants;

public static class Containers
{
    public const string ProfileImages = "profile-images";

    public const string DialogImages = "dialog-images";
}