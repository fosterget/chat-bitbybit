﻿using System.Runtime.InteropServices.ComTypes;
using System.Text;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Repositories;
using Messanger.Core.Models.Bots;

namespace Messanger.Infrastructure.Generators;

public class BotResultsCsvGenerator : IBotResultsCsvGenerator
{
    private readonly IBotDataRepository _botDataRepository;

    private readonly IUnitOfWork _unitOfWork;

    public BotResultsCsvGenerator(IBotDataRepository botDataRepository, IUnitOfWork unitOfWork)
    {
        _botDataRepository = botDataRepository;
        _unitOfWork = unitOfWork;
    }
    
    public async Task<BotResults> GetResults(Guid botId)
    {
        var botResults = _botDataRepository.GetBotResults(botId);
        var botSteps = _unitOfWork.Steps.GetWhereAsync(step => step.BotId == botId && step.SaveData == true);
        await Task.WhenAll(botResults, botSteps);

        var csvContent = new StringBuilder("name,email");
        foreach (var step in botSteps.Result)
        {
            csvContent.Append("," + step.Name);
        }
        
        foreach (var user in botResults.Result)
        {
            csvContent.AppendLine("");
            var userEntity = await _unitOfWork.Users.GetByIdAsync(new Guid(user["id"]));
            csvContent.Append($"\"{userEntity.Name} {userEntity.Surname}\",\"{userEntity.Email}\"");

            foreach (var step in botSteps.Result)
            {
                if (user.ContainsKey(step.Name))
                {
                    csvContent.Append($",\"{user[step.Name]}\"");
                }
                else
                {
                    csvContent.Append(',');
                }
            }
        }

        return new BotResults
        {
            Name = "BotResults.csv",
            ContentType = "text/csv",
            Buffer = Encoding.ASCII.GetBytes(csvContent.ToString())
        };
    }
}