﻿using Messanger.Core.Entities;
using Messanger.Core.Interfaces.Repositories;

namespace Messanger.Infrastructure.Repositories;

public class InterlocutorRepository : GenericRepository<Interlocutor>, IInterlocutorRepository
{
    public InterlocutorRepository(ApplicationDbContext context) : base(context)
    {
    }
}