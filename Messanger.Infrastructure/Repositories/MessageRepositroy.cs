﻿using Messanger.Core.Entities;
using Messanger.Core.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Messanger.Infrastructure.Repositories
{
    public class MessageRepositroy : GenericRepository<Message>, IMessageRepository
    {
        public MessageRepositroy(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Message>> GetByDialogId(Guid dialogId, int offset, int limit)
        {
            return await _context.Set<Message>()
                .Where(m => m.DialogId == dialogId)
                .Include(m => m.Author)
                .Include(m => m.Buttons)
                .Include(m => m.File)
                .OrderByDescending(m => m.Date)
                .Skip(offset)
                .Take(limit)
                .Reverse()
                .ToListAsync();
        }
    }
}
