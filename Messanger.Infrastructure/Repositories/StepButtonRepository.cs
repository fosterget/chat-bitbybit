﻿using Messanger.Core.Entities;
using Messanger.Core.Interfaces.Repositories;

namespace Messanger.Infrastructure.Repositories;

public class StepButtonRepository : GenericRepository<StepButton>, IStepButtonRepository
{
    public StepButtonRepository(ApplicationDbContext context) : base(context)
    {
    }
}