using Messanger.Core.Entities;
using Messanger.Core.Enums;
using Messanger.Core.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Messanger.Infrastructure.Repositories
{
    public class DialogRepository : GenericRepository<Dialog>, IDialogRepository
    {
        public DialogRepository(ApplicationDbContext context) : base(context) 
        {
        }

        public async Task<IEnumerable<Dialog>> GetByUser(User user, DialogType? type = null)
        {
            var query = _context.Set<Dialog>()
                .Include(d => d.Interlocutors)
                .Include(d => d.Team)
                .Where(d => d.Interlocutors.Contains(user));

            if (type is not null)
            {
                query = query.Where(dialog => dialog.Type == type);
            }

            return await query.ToListAsync();
        }
        
        public async Task<IEnumerable<Dialog>> GetByUserId(Guid userId)
        {
            var dialogs = await _context.Set<Dialog>()
                .Where(d => d.Interlocutors.Any(i => i.Id == userId))
                .Include(d => d.Interlocutors)
                .ToListAsync();

            return dialogs;
        }

        public async Task<Dialog?> GetWithMessagesAndUsers(Guid dialogId)
        {
            return await _context.Set<Dialog>()
                .Include(d => d.Messages)
                .Include(d => d.Interlocutors)
                .FirstOrDefaultAsync(d => d.Id == dialogId);
        }
    }
}
