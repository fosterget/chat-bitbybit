﻿using Messanger.Core.Interfaces.Repositories;
using File = Messanger.Core.Entities.File;

namespace Messanger.Infrastructure.Repositories;

public class FileRepository : GenericRepository<File>, IFileRepository
{
    public FileRepository(ApplicationDbContext context) : base(context)
    {
    }
}