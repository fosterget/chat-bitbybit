using Messanger.Core.Entities;
using Messanger.Core.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Messanger.Infrastructure.Repositories;

public class TeamRepository : GenericRepository<Team>, ITeamRepository
{
    public TeamRepository(ApplicationDbContext context) : base(context)
    {
    }

    public async Task<Team> GetTeamWithUsersAsync(Guid teamId)
    {
        return await _context.Set<Team>()
            .Include(team => team.Users)
            .FirstAsync(team => team.Id == teamId);
    }

    public async Task<IEnumerable<Team>> GetTeamsByOwnerAsync(Guid ownerId)
    {
        return await _context.Set<Team>()
            .Include(team => team.Owner)
            .Where(team => team.OwnerId == ownerId)
            .ToListAsync();
    }

    public async Task<IEnumerable<User>> GetTeamUsers(Guid teamId)
    {
        return await _context.Set<Team>()
            .Include(team => team.Users)
            .Where(team => team.Id == teamId )
            .Select(team => team.Users)
            .FirstAsync();
    }
}