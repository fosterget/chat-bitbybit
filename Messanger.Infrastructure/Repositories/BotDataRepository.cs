﻿using Json.Net;
using Messanger.Core.Interfaces.Repositories;
using StackExchange.Redis;

namespace Messanger.Infrastructure.Repositories;

public class BotDataRepository : IBotDataRepository
{
    private readonly IConnectionMultiplexer _redis;
    
    public BotDataRepository(IConnectionMultiplexer redis)
    {
        _redis = redis;
    }
    
    public async Task CreateBotAsync(Guid botId, Guid userId)
    {
        var hashKey = botId.ToString();

        var db = _redis.GetDatabase();
        await db.HashSetAsync(hashKey, Array.Empty<HashEntry>());
    }

    public async Task<Dictionary<string, string>> GetUserDataAsync(Guid botId, Guid userId)
    {
        var db = _redis.GetDatabase();
        var userEntry = await db.HashGetAsync(botId.ToString(), userId.ToString());

        if (!userEntry.HasValue)
            return new Dictionary<string, string>();
        
        var userData = JsonNet.Deserialize<Dictionary<string, string>>(userEntry);
        return userData;
    }

    public async Task SaveUserDataAsync(Guid botId, Guid userId, Dictionary<string, string> data)
    {
        var hashKey = botId.ToString();
        var db = _redis.GetDatabase();
        await db.HashSetAsync(hashKey, userId.ToString(), JsonNet.Serialize(data));
    }

    public async Task<List<Dictionary<string, string>>> GetBotResults(Guid botId)
    {
        var hashKey = botId.ToString();
        var db = _redis.GetDatabase();
        var usersEntry = await db.HashGetAllAsync(hashKey);

        var users = new List<Dictionary<string, string>>();
        foreach (var entry in usersEntry)
        {
            var user = JsonNet.Deserialize<Dictionary<string, string>>(entry.Value);
            user["id"] = entry.Name.ToString();
            users.Add(user);
        }

        return users;
    }
}