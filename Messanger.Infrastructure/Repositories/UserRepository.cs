﻿using Messanger.Core.Entities;
using Messanger.Core.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Messanger.Infrastructure.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<User?> FindByEmailAsync(string email)
        {
            return await _context.Set<User>()
                .Include(u => u.RefreshTokens)
                .FirstOrDefaultAsync(u => u.Email == email);
        }

        public async Task<bool> IsTokenUniqueAsync(string token)
        {
            return !await _context.Set<User>()
                .Include(u => u.RefreshTokens)
                .AnyAsync(u => u.RefreshTokens.Any(t => t.Token == token));
        }

        public async Task<User?> GetUserByRefreshTokenAsync(string token)
        {
            return await _context.Set<User>()
                .Include(u => u.RefreshTokens)
                .SingleOrDefaultAsync(u => u.RefreshTokens.Any(t => t.Token == token));
        }

        public async Task<User?> GetUserWithDialogsByIdAsync(Guid userId)
        {
            return await _context.Set<User>()
                .Include(u => u.Dialogs).ThenInclude(d => d.Messages)
                .SingleOrDefaultAsync(u => u.Id == userId);
        }

        public async Task<IEnumerable<User>> FindByNameAndSurname(string query, Guid userIdToExclude)
        {
            return await _context.Set<User>()
                .Where(u => (u.Name + " " + u.Surname).ToLower().Contains(query.ToLower()) && u.Id != userIdToExclude)
                .ToListAsync();
        }

        public async Task<User?> GetUserWithContacts(Guid userId)
        {
            return await _context.Set<User>()
                .Where(u => u.Id == userId)
                .Include(u => u.Contacts)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<User>> GetUserContacts(Guid userId)
        {
            return await _context.Set<User>()
                .Where(u => u.Id == userId)
                .Include(u => u.Contacts)
                .Select(u => u.Contacts)
                .FirstAsync();
        }
    }
}
