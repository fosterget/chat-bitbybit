﻿using Messanger.Core.Entities;
using Messanger.Core.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Messanger.Infrastructure.Repositories;

public class StepRepository : GenericRepository<Step>, IStepRepository
{
    public StepRepository(ApplicationDbContext context) : base(context)
    {
    }

    public async Task<Step?> GetByIdWithSteps(Guid id)
    {
        return await _context.Set<Step>()
            .Include(s => s.Buttons)!.ThenInclude(b => b.NextStep)
            .Include(s => s.NextStep).ThenInclude(s => s!.Buttons)
            .FirstOrDefaultAsync(s => s.Id == id);
    }

    public async Task<Step?> GetByBotIdWithSteps(Guid id)
    {
        return await _context.Set<Step>()
            .Include(s => s.Buttons)!.ThenInclude(b => b.NextStep)
            .Include(s => s.NextStep).ThenInclude(s => s!.Buttons)
            .FirstOrDefaultAsync(s => s.BotId == id);
    }
}