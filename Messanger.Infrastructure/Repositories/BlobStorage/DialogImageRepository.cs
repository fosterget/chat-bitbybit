﻿using Azure.Storage.Blobs;
using Messanger.Core.Interfaces.Repositories.BlobStorage;

namespace Messanger.Infrastructure.Repositories.BlobStorage;

public class DialogImageRepository : BlobRepository, IDialogImageRepository
{
    protected override string ContainerName => Constants.Containers.DialogImages;
    
    public DialogImageRepository(BlobServiceClient blobServiceClient) : base(blobServiceClient)
    {
    }
}