using Azure.Storage.Blobs;
using Messanger.Core.Interfaces.Repositories.BlobStorage;

namespace Messanger.Infrastructure.Repositories.BlobStorage;

public class ProfileImageRepository : BlobRepository, IProfileImageRepository
{
    protected override string ContainerName => Constants.Containers.ProfileImages;

    public ProfileImageRepository(BlobServiceClient blobServiceClient) : base(blobServiceClient)
    {
    }
}