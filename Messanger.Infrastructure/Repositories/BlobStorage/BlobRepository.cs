using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Messanger.Core.Interfaces.Repositories.BlobStorage;
using Messanger.Core.Models.Common;
using Microsoft.AspNetCore.Http;

namespace Messanger.Infrastructure.Repositories.BlobStorage;

public abstract class BlobRepository : IBlobRepository
{
    protected abstract string ContainerName { get; }

    private readonly BlobServiceClient _blobServiceClient;

    public BlobRepository(BlobServiceClient blobServiceClient)
    {
        _blobServiceClient = blobServiceClient;
    }

    public async Task<FileDTO> GetFile(string name)
    {
        var containerClient = _blobServiceClient.GetBlobContainerClient(ContainerName);
        var blobClient = containerClient.GetBlobClient(name);
        var downloadedBlob = await blobClient.DownloadAsync();
        
        return new FileDTO
        {
            Content = downloadedBlob.Value.Content,
            ContentType = downloadedBlob.Value.ContentType
        };
    }
    
    public async Task<string> AddFile(IFormFile file, string filename = "")
    {
        var fullFilename = string.IsNullOrWhiteSpace(filename)
            ? file.FileName
            : filename;
        
        var containerClient = _blobServiceClient.GetBlobContainerClient(ContainerName);
        var blobClient = containerClient.GetBlobClient(fullFilename);
        var stream = file.OpenReadStream();
        await blobClient.UploadAsync(stream, new BlobHttpHeaders {ContentType = file.ContentType});

        return blobClient.Uri.AbsoluteUri;
    }
}