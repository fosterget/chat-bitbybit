using Messanger.Core.Entities;
using Messanger.Core.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Messanger.Infrastructure.Repositories;

public class BotRepository : GenericRepository<Bot>, IBotRepository
{
    public BotRepository(ApplicationDbContext context) : base(context)
    {
    }

    public async Task<Bot?> GetBotByIdAsync(Guid botId)
    {
        return await _context.Set<Bot>()
        .Include(b => b.Steps).ThenInclude(s => s.Buttons)
        .Include(b => b.Steps).ThenInclude(s => s.NextStep)
        .FirstOrDefaultAsync(b => b.Id == botId);
    }
}