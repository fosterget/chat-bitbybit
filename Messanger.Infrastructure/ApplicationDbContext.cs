using Messanger.Core.Constants;
using Messanger.Core.Entities;
using Messanger.Core.Enums;
using Microsoft.EntityFrameworkCore;
using File = Messanger.Core.Entities.File;

namespace Messanger.Infrastructure
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Interlocutor>(e =>
            {
                e.HasKey(p => p.Id);
                e.Property(p => p.Name).IsRequired();
                e.Property(p => p.CreationDate).IsRequired();
                e.Property(p => p.Type)
                    .HasConversion(
                        v => v.ToString(),
                        v => (InterlocutorType)Enum.Parse(typeof(InterlocutorType), v));
            });
            
            builder.Entity<User>(e =>
            {
                e.ToTable("Users");
                e.HasMany(p => p.Contacts);
                e.Property(p => p.Surname).IsRequired();
                e.Property(p => p.Email).IsRequired();
                e.Property(p => p.Password).IsRequired();
            });

            builder.Entity<RefreshToken>(e =>
            {
                e.HasKey(p => p.Id);
                e.HasOne(p => p.User)
                    .WithMany(p => p.RefreshTokens)
                    .HasForeignKey(p => p.UserId);
            });

            builder.Entity<Dialog>(e =>
            {
                e.HasKey(p => p.Id);
                    e.HasMany(p => p.Interlocutors)
                        .WithMany(p => p.Dialogs);
                e.Property(p => p.Type)
                    .HasConversion(
                        v => v.ToString(),
                        v => (DialogType)Enum.Parse(typeof(DialogType), v));
            });

            builder.Entity<Message>(e =>
            {
                e.HasKey(p => p.Id);
                e.Property(p => p.Date).IsRequired();
                e.HasOne(p => p.Author)
                    .WithMany(p => p.Messages)
                    .HasForeignKey(p => p.AuthorId);
                e.HasOne(p => p.Dialog)
                    .WithMany(p => p.Messages)
                    .HasForeignKey(p => p.DialogId);
            });

            builder.Entity<MessageButton>(e =>
            {
                e.HasKey(p => p.Id);
                e.Property(p => p.Title).IsRequired();
                e.HasOne(p => p.Message)
                    .WithMany(p => p.Buttons)
                    .HasForeignKey(p => p.MessageId);
            });

            builder.Entity<Bot>(e =>
            {
                e.ToTable("Bot");
                e.HasOne(p => p.Creator)
                    .WithMany(p => p.Bots)
                    .HasForeignKey(p => p.CreatorId);
            });

            builder.Entity<Step>(e =>
            {
                e.HasKey(p => p.Id);
                e.Property(p => p.Text).IsRequired(); 
                e.Property(p => p.Name).IsRequired(); 
                e.HasOne(p => p.Bot)
                    .WithMany(p => p.Steps)
                    .HasForeignKey(p => p.BotId);
                e.Property(p => p.AnswerType)
                    .HasConversion(
                        v => v.ToString(),
                        v => (AnswerType)Enum.Parse(typeof(AnswerType), v));
            });

            builder.Entity<StepButton>(e =>
            {
                e.HasKey(p => p.Id);
                e.Property(p => p.Title).IsRequired();
                e.HasOne(p => p.Step)
                    .WithMany(p => p.Buttons)
                    .HasForeignKey(p => p.StepId);
                e.HasOne(p => p.NextStep);
            });
            
            builder.Entity<Team>(e =>
            {
                e.HasKey(p => p.Id);
                e.Property(p => p.Name).IsRequired();
                e.HasMany(p => p.Users)
                    .WithMany(p => p.Teams);
                e.HasOne(p => p.Dialog)
                    .WithOne(p => p.Team)
                    .HasForeignKey<Team>(p => p.DialogId);
                e.HasOne(p => p.Owner)
                    .WithMany(p => p.CreatedTeams)
                    .HasForeignKey(p => p.OwnerId);
            });

            builder.Entity<File>(e =>
            {
                e.HasKey(p => p.Id);
                e.HasOne(p => p.Message)
                    .WithOne(p => p.File)
                    .HasForeignKey<Message>(p => p.FileId);
                e.Property(p => p.Type)
                    .HasConversion(
                        v => v.ToString(),
                        v => (FileType)Enum.Parse(typeof(FileType), v));
            });
        }
        
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            foreach (var item in ChangeTracker.Entries().Where(x=>x.Entity is Interlocutor && x.State is EntityState.Added))
            {
                ((Interlocutor)item.Entity).Type = (InterlocutorType)Enum.Parse(typeof(InterlocutorType), item.Entity.GetType().Name);
            }
            return base.SaveChangesAsync(cancellationToken);
        }
        
        public override int SaveChanges()
        {
            foreach (var item in ChangeTracker.Entries().Where(x=>x.Entity is Interlocutor && x.State is EntityState.Added))
            {
                ((Interlocutor)item.Entity).Type = (InterlocutorType)Enum.Parse(typeof(InterlocutorType), item.Entity.GetType().Name);
            }
            return base.SaveChanges();
        }
    }
}
