﻿// <auto-generated />
using System;
using Messanger.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace Messanger.Infrastructure.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20220604185700_AddDialogType")]
    partial class AddDialogType
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.1")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("DialogInterlocutor", b =>
                {
                    b.Property<Guid>("DialogsId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("InterlocutorsId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("DialogsId", "InterlocutorsId");

                    b.HasIndex("InterlocutorsId");

                    b.ToTable("DialogInterlocutor");
                });

            modelBuilder.Entity("Messanger.Core.Entities.Dialog", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("LastMessageText")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Type")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.ToTable("Dialogs");
                });

            modelBuilder.Entity("Messanger.Core.Entities.Interlocutor", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("CreationDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("ProfileImageUpdated")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.ToTable("Interlocutors");
                });

            modelBuilder.Entity("Messanger.Core.Entities.Message", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("AuthorId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("Date")
                        .HasColumnType("datetime2");

                    b.Property<Guid>("DialogId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Text")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("AuthorId");

                    b.HasIndex("DialogId");

                    b.ToTable("Messages");
                });

            modelBuilder.Entity("Messanger.Core.Entities.MessageButton", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("MessageId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("MessageId");

                    b.ToTable("MessageButton");
                });

            modelBuilder.Entity("Messanger.Core.Entities.RefreshToken", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<string>("CreatedByIp")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Expires")
                        .HasColumnType("datetime2");

                    b.Property<string>("ReasonRevoked")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ReplacedByToken")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("Revoked")
                        .HasColumnType("datetime2");

                    b.Property<string>("RevokedByIp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Token")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("RefreshTokens");
                });

            modelBuilder.Entity("Messanger.Core.Entities.Step", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("AnswerType")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("BotId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid?>("NextStepId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Text")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("BotId");

                    b.HasIndex("NextStepId");

                    b.ToTable("Step");
                });

            modelBuilder.Entity("Messanger.Core.Entities.StepButton", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid?>("NextStepId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("SaveData")
                        .HasColumnType("bit");

                    b.Property<Guid>("StepId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("NextStepId");

                    b.HasIndex("StepId");

                    b.ToTable("StepButton");
                });

            modelBuilder.Entity("UserUser", b =>
                {
                    b.Property<Guid>("ContactsId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("InContactsId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("ContactsId", "InContactsId");

                    b.HasIndex("InContactsId");

                    b.ToTable("UserUser");
                });

            modelBuilder.Entity("Messanger.Core.Entities.Bot", b =>
                {
                    b.HasBaseType("Messanger.Core.Entities.Interlocutor");

                    b.Property<Guid>("CreatorId")
                        .HasColumnType("uniqueidentifier");

                    b.HasIndex("CreatorId");

                    b.ToTable("Bot", (string)null);
                });

            modelBuilder.Entity("Messanger.Core.Entities.User", b =>
                {
                    b.HasBaseType("Messanger.Core.Entities.Interlocutor");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Surname")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.ToTable("Users", (string)null);
                });

            modelBuilder.Entity("DialogInterlocutor", b =>
                {
                    b.HasOne("Messanger.Core.Entities.Dialog", null)
                        .WithMany()
                        .HasForeignKey("DialogsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Messanger.Core.Entities.Interlocutor", null)
                        .WithMany()
                        .HasForeignKey("InterlocutorsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Messanger.Core.Entities.Message", b =>
                {
                    b.HasOne("Messanger.Core.Entities.Interlocutor", "Author")
                        .WithMany("Messages")
                        .HasForeignKey("AuthorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Messanger.Core.Entities.Dialog", "Dialog")
                        .WithMany("Messages")
                        .HasForeignKey("DialogId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Author");

                    b.Navigation("Dialog");
                });

            modelBuilder.Entity("Messanger.Core.Entities.MessageButton", b =>
                {
                    b.HasOne("Messanger.Core.Entities.Message", "Message")
                        .WithMany("Buttons")
                        .HasForeignKey("MessageId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Message");
                });

            modelBuilder.Entity("Messanger.Core.Entities.RefreshToken", b =>
                {
                    b.HasOne("Messanger.Core.Entities.User", "User")
                        .WithMany("RefreshTokens")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("User");
                });

            modelBuilder.Entity("Messanger.Core.Entities.Step", b =>
                {
                    b.HasOne("Messanger.Core.Entities.Bot", "Bot")
                        .WithMany("Steps")
                        .HasForeignKey("BotId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Messanger.Core.Entities.Step", "NextStep")
                        .WithMany()
                        .HasForeignKey("NextStepId");

                    b.Navigation("Bot");

                    b.Navigation("NextStep");
                });

            modelBuilder.Entity("Messanger.Core.Entities.StepButton", b =>
                {
                    b.HasOne("Messanger.Core.Entities.Step", "NextStep")
                        .WithMany()
                        .HasForeignKey("NextStepId");

                    b.HasOne("Messanger.Core.Entities.Step", "Step")
                        .WithMany("Buttons")
                        .HasForeignKey("StepId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("NextStep");

                    b.Navigation("Step");
                });

            modelBuilder.Entity("UserUser", b =>
                {
                    b.HasOne("Messanger.Core.Entities.User", null)
                        .WithMany()
                        .HasForeignKey("ContactsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Messanger.Core.Entities.User", null)
                        .WithMany()
                        .HasForeignKey("InContactsId")
                        .OnDelete(DeleteBehavior.ClientCascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Messanger.Core.Entities.Bot", b =>
                {
                    b.HasOne("Messanger.Core.Entities.User", "Creator")
                        .WithMany("Bots")
                        .HasForeignKey("CreatorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Messanger.Core.Entities.Interlocutor", null)
                        .WithOne()
                        .HasForeignKey("Messanger.Core.Entities.Bot", "Id")
                        .OnDelete(DeleteBehavior.ClientCascade)
                        .IsRequired();

                    b.Navigation("Creator");
                });

            modelBuilder.Entity("Messanger.Core.Entities.User", b =>
                {
                    b.HasOne("Messanger.Core.Entities.Interlocutor", null)
                        .WithOne()
                        .HasForeignKey("Messanger.Core.Entities.User", "Id")
                        .OnDelete(DeleteBehavior.ClientCascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Messanger.Core.Entities.Dialog", b =>
                {
                    b.Navigation("Messages");
                });

            modelBuilder.Entity("Messanger.Core.Entities.Interlocutor", b =>
                {
                    b.Navigation("Messages");
                });

            modelBuilder.Entity("Messanger.Core.Entities.Message", b =>
                {
                    b.Navigation("Buttons");
                });

            modelBuilder.Entity("Messanger.Core.Entities.Step", b =>
                {
                    b.Navigation("Buttons");
                });

            modelBuilder.Entity("Messanger.Core.Entities.Bot", b =>
                {
                    b.Navigation("Steps");
                });

            modelBuilder.Entity("Messanger.Core.Entities.User", b =>
                {
                    b.Navigation("Bots");

                    b.Navigation("RefreshTokens");
                });
#pragma warning restore 612, 618
        }
    }
}
