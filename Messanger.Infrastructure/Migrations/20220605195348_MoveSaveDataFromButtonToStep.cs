﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Messanger.Infrastructure.Migrations
{
    public partial class MoveSaveDataFromButtonToStep : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SaveData",
                table: "StepButton");

            migrationBuilder.AddColumn<bool>(
                name: "SaveData",
                table: "Step",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SaveData",
                table: "Step");

            migrationBuilder.AddColumn<bool>(
                name: "SaveData",
                table: "StepButton",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
