using Messanger.Core.Models.Auth;
using Messanger.Infrastructure;
using Messanger.WebApp.Configurations;
using Messanger.WebApp.Hubs;
using Messanger.WebApp.Middleware;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.Configure<JwtSettings>(builder.Configuration.GetSection("JwtSettings"));

builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
});

builder.Services.AddMapper();
// Add services from DIConfig.cs
builder.Services.AddAppServices();
builder.Services.AddInfrastructure(builder.Configuration);
builder.Services.AddAuthenticationConfiguration(builder.Configuration);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerConfiguration();
builder.Services.AddSignalR(e =>
{
e.EnableDetailedErrors = true;
    e.MaximumReceiveMessageSize = 102400000;
});

var app = builder.Build();

app.UseForwardedHeaders(new ForwardedHeadersOptions
{
    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
});

app.UseMiddleware<ErrorHandlerMiddleware>();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseRouting();
app.UseHttpsRedirection();

app.UseCors(x => x
    //.WithOrigins("http://localhost:3000", "http://localhost:3000", "http://chat.bitbybit.com.ua", "https://chat.bitbybit.com.ua")
    //.SetIsOriginAllowed(host => true)
    .AllowAnyMethod()
    .AllowAnyHeader()
    .AllowAnyOrigin());

app.UseAuthentication();
app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapHub<ChatHub>("/chatHub");
    endpoints.MapControllers();
});

using (var scope = app.Services.CreateScope())
{
    await scope.ServiceProvider.GetService<ApplicationDbContext>()!.Database.MigrateAsync();
}

app.Run();
