﻿using System.ComponentModel.DataAnnotations;
using Messanger.Core.Constants;

namespace Messanger.WebApp.Models.Account;

public class EditProfileModel
{
    [Required]
    [MaxLength(AccountConstants.NameMaxLength)]
    [MinLength(AccountConstants.NameMinLength)]
    public string Name { get; set; }
        
    [Required]
    [MaxLength(AccountConstants.SurnameMaxLength)]
    [MinLength(AccountConstants.SurnameMinLength)]
    public string Surname { get; set; }
        
    [EmailAddress]
    public string Email { get; set; }
    
    [Required]
    [MaxLength(AccountConstants.PositionMaxLength)]
    [MinLength(AccountConstants.PositionMinLength)]
    public string Position { get; set; }
    
    [Required]
    [MaxLength(AccountConstants.BioMaxLength)]
    [MinLength(AccountConstants.BioMinLength)]
    public string Bio { get; set; }
}