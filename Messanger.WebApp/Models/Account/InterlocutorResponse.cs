﻿namespace Messanger.WebApp.Models.Account;

public class InterlocutorResponse
{
    public Guid Id { get; set; }

    public string Name { get; set; }
    
    public DateTime CreationDate { get; init; }
        
    public DateTime ProfileImageUpdated { get; set; }
    
    public string? ProfileImageUrl { get; set; }
}