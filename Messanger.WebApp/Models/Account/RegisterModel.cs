﻿using System.ComponentModel.DataAnnotations;
using Messanger.Core.Constants;

namespace Messanger.WebApp.Models.Account
{
    public class RegisterModel
    {
        [Required]
        [MaxLength(AccountConstants.NameMaxLength)]
        public string Name { get; set; }
        
        [Required]
        [MaxLength(AccountConstants.SurnameMaxLength)]
        public string Surname { get; set; }
        
        [EmailAddress]
        public string Email { get; set; }
        
        [MinLength(AccountConstants.PasswordMinLength)]
        [MaxLength(AccountConstants.PasswordMaxLength)]
        public string Password { get; set; }
    }
}
