﻿namespace Messanger.WebApp.Models.Account
{
    public class UserResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        
        public string Surname { get; set; }
        
        public string? Position { get; set; }
        
        public string? Bio { get; set; }
        
        public string Email { get; set; }
        
        public DateTime ProfileImageUpdated { get; set; }
        
        public string? ProfileImageUrl { get; set; }
    }
}
