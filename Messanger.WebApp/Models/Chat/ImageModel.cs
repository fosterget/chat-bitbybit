﻿using Microsoft.AspNetCore.Mvc;

namespace Messanger.WebApp.Models.Chat;

public class ImageModel
{
    [FromForm(Name="File")]
    public IFormFile File { get; set; }
    
    [FromForm(Name="DialogId")]
    public Guid DialogId { get; set; }
}