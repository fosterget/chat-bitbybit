﻿using Messanger.Core.Enums;

namespace Messanger.WebApp.Models.Chat;

public class FileResponse
{
    public Guid Id { get; set; }
    
    public string Name { get; set; }
    
    public string ContentType { get; set; }
    
    public string? Url { get; set; }

    public FileType Type { get; set; }
}