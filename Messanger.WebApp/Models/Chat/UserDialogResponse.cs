﻿using Messanger.WebApp.Models.Account;

namespace Messanger.WebApp.Models.Chat
{
    public class UserDialogResponse
    {
        public Guid Id { get; set; }

        public string LastMessageText { get; set; }

        public DateTime Updated { get; set; }

        public IEnumerable<UserResponse> Interlocutors { get; set; }
    }
}
