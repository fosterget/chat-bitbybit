﻿using Messanger.WebApp.Models.Account;
using Messanger.WebApp.Models.Team;

namespace Messanger.WebApp.Models.Chat;

public class GroupDialogResponse
{
    public Guid Id { get; set; }

    public string LastMessageText { get; set; }

    public DateTime Updated { get; set; }
        
    public TeamResponse Team { get; set; }

    public IEnumerable<UserResponse> Interlocutors { get; set; }
}