﻿using Messanger.WebApp.Models.Account;

namespace Messanger.WebApp.Models.Chat
{
    public class MessageResponse
    {
        public Guid Id { get; set; }

        public string Text { get; set; }

        public DateTime Date { get; set; }

        public InterlocutorResponse Author { get; set; }
        
        public Guid DialogId { get; set; }
        
        public FileResponse? File { get; set; }
        
        public BotDialogResponse Dialog { get; set; }

        public ICollection<ButtonResponse> Buttons { get; set; }
    }
}
