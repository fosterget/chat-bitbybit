﻿using Messanger.Core.Enums;
using Messanger.WebApp.Models.Account;

namespace Messanger.WebApp.Models.Chat;

public class BotDialogResponse
{
    public Guid Id { get; set; }

    public string LastMessageText { get; set; }

    public DateTime Updated { get; set; }

    public IEnumerable<InterlocutorResponse> Interlocutors { get; set; }

    public DialogType DialogType { get; set; }
}