﻿using System.ComponentModel.DataAnnotations;

namespace Messanger.WebApp.Models.Chat
{
    public class MessageModel
    {
        [MinLength(1)]
        public string Text { get; set; }

        public Guid DialogId { get; set; }

        public Guid RecipientId { get; set; }
    }
}
