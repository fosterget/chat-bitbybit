﻿using System.ComponentModel.DataAnnotations;
using Messanger.Core.Constants;

namespace Messanger.WebApp.Models.Auth
{
    public class AuthenticateModel
    {
        [EmailAddress]
        public string Email { get; set;}
        
        [MinLength(AccountConstants.PasswordMinLength)]
        [MaxLength(AccountConstants.PasswordMaxLength)]
        public string Password { get; set;}
    }
}
