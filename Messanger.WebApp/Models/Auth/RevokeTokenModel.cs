﻿namespace Messanger.WebApp.Models.Auth
{
    public class RevokeTokenModel
    {
        public string? Token { get; set; }
    }
}
