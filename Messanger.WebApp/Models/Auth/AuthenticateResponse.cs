﻿namespace Messanger.WebApp.Models.Auth
{
    public class AuthenticateResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        
        public string Surname { get; set; }
        
        public string Email { get; set; }

        public string AccessToken { get; set; }
        
        public DateTime ProfileImageUpdated { get; set; }
    }
}
