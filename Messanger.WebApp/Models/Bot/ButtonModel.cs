﻿namespace Messanger.WebApp.Models.Bot;

public class ButtonModel
{
    public string Title { get; set; }

    public int? NextStepId { get; set; }
}