﻿using Messanger.Core.Enums;

namespace Messanger.WebApp.Models.Bot;

public class StepModel
{
    public int Id { get; set; }
    
    public string Name { get; set; }
    
    public string Text { get; set; }

    public int? NextStepId { get; set; }

    public AnswerType AnswerType { get; set; }

    // public bool SaveData { get; set; } = true;
    public bool SaveData { get; set; }

    public IEnumerable<ButtonModel> Buttons { get; set; }
}