﻿namespace Messanger.WebApp.Models.Bot;

public class BotModel
{
    public string Name { get; set; }

    public IEnumerable<StepModel> Steps { get; set; }
}