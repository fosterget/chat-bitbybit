namespace Messanger.WebApp.Models.Team;

public class TeamUserModel
{
    public string TeamId { get; set; }
    
    public string UserId { get; set; }
}