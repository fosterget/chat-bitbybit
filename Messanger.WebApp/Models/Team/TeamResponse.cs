﻿using Messanger.WebApp.Models.Account;

namespace Messanger.WebApp.Models.Team;

public class TeamResponse
{
    public Guid Id { get; set; }

    public string Name { get; set; }
    
    public Guid? OwnerId { get; set; }
    
    public UserResponse? Owner { get; set; }

    public ICollection<UserResponse> Users { get; set; }
}