namespace Messanger.WebApp.Models.Team;

public class TeamModel
{
    public string Name { get; set; }
    
    public List<string> UserIds { get; set; }
}