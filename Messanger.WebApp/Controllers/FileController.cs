using Messanger.Core.Constants;
using Messanger.Core.Interfaces.Services.Account;
using Messanger.WebApp.Attributes;
using Microsoft.AspNetCore.Mvc;

namespace Messanger.WebApp.Controllers;

[Route("api/[controller]")]
[ApiController]
// [Authorize]
public class FileController : ControllerBase
{
    private readonly IProfileFinder _profileFinder;

    public FileController(IProfileFinder profileFinder)
    {
        _profileFinder = profileFinder;
    }
    
    [Route("getProfileImage")]
    [HttpGet]
    public async Task<IActionResult> GetProfileImage([FromQuery]string userId)
    {
        var file = await _profileFinder.GetUserProfileImage(new Guid(userId));
        return base.File(file.Content, file.ContentType);
    }
}