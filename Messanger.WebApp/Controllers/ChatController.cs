﻿using AutoMapper;
using Messanger.Core.Constants;
using Messanger.Core.Entities;
using Messanger.Core.Enums;
using Messanger.Core.Exceptions;
using Messanger.Core.Interfaces.Services.Chat;
using Messanger.Core.Models.Chat;
using Messanger.WebApp.Attributes;
//using Messanger.WebApp.Attributes;
using Messanger.WebApp.Models.Chat;
using Microsoft.AspNetCore.Mvc;

namespace Messanger.WebApp.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ChatController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly IDialogsFinder _dialogsFinder;

        private readonly IMessageSender _messageSender;

        private readonly IMessageFinder _messageFinder;

        public ChatController(
            IMapper mapper, 
            IDialogsFinder dialogsFinder,
            IMessageSender messageSender,
            IMessageFinder messageFinder)
        {
            _mapper = mapper;
            _dialogsFinder = dialogsFinder;
            _messageSender = messageSender;
            _messageFinder = messageFinder;
        }

        [Route("dialogs")]
        [HttpGet]
        public async Task<IActionResult> GetDialogs(DialogType type)
        {
            var userId = Guid.Parse(User.Claims.First(c => c.Type == AuthConstants.Claims.Id).Value);
            var dialogs = await _dialogsFinder.GetPersonalDialogsAsync(userId, type);
            
            if (type is DialogType.Bot)
            {
                return Ok(_mapper.Map<IEnumerable<BotDialogResponse>>(dialogs));
            }

            if (type is DialogType.Group)
            {
                return Ok(_mapper.Map<IEnumerable<GroupDialogResponse>>(dialogs));
            }

            return Ok(_mapper.Map<IEnumerable<UserDialogResponse>>(dialogs));
        }

        [Route("messages")]
        [HttpGet]
        public async Task<IActionResult> GetMessagesByDialog(Guid dialogId, int offset, int limit)
        {
            var userId = Guid.Parse(User.Claims.First(c => c.Type == AuthConstants.Claims.Id).Value);
            var messages = await _messageFinder.GetMessagesAsync(
                userId,
                dialogId,
                new PaginationInfo(offset, limit));

            var result = _mapper.Map<IEnumerable<MessageResponse>>(messages);

            return Ok(result);
        }

        [Route("sendMessage")]
        [HttpPost]
        public async Task<IActionResult> SendMessage([FromBody] MessageModel model)
        {
            var userId = Guid.Parse(User.Claims.First(c => c.Type == AuthConstants.Claims.Id).Value);
            var messageInfo = _mapper.Map<MessageInfo>(model);
            messageInfo.AuthorId = userId;

            Message message;
            if (model.DialogId != default)
            {
                message = await _messageSender.SendMessageByDialogAsync(messageInfo);
            } else if (model.RecipientId != default)
            {
                message = await _messageSender.SendMessageByRecipientAsync(messageInfo);
            }
            else
            {
                throw new InvalidArgumentException(
                    $"{nameof(model.DialogId)} or {nameof(model.RecipientId)} should be initialized");
            }
            
            var response = _mapper.Map<MessageResponse>(message);
            
            return Ok(response);
        }

        [Route("SendImage")]
        [HttpPost]
        public async Task<IActionResult> SendImage([FromForm] ImageModel model)
        {
            var userId = Guid.Parse(User.Claims.First(c => c.Type == AuthConstants.Claims.Id).Value);

            var message = await _messageSender.SendImage(userId, model.DialogId, model.File);

            return Ok(_mapper.Map<MessageResponse>(message));
        }
    }
}
