﻿using AutoMapper;
using Messanger.Core.Constants;
using Messanger.Core.Interfaces.Services.Account;
using Messanger.Core.Models.Account;
using Messanger.WebApp.Attributes;
using Messanger.WebApp.Models.Account;
using Microsoft.AspNetCore.Mvc;

namespace Messanger.WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IMapper _mapper;
        
        private readonly IRegistrar _registrar;

        private readonly IProfileEditor _profileEditor;

        private readonly IProfileFinder _profileFinder;

        private readonly IContactAdder _contactAdder;

        private readonly IContactsFinder _contactsFinder;

        public AccountController(
            IMapper mapper,
            IRegistrar registrar,
            IProfileEditor profileEditor,
            IProfileFinder profileFinder,
            IContactAdder contactAdder,
            IContactsFinder contactsFinder)
        {
            _mapper = mapper;
            _registrar = registrar;
            _profileEditor = profileEditor;
            _profileFinder = profileFinder;
            _contactAdder = contactAdder;
            _contactsFinder = contactsFinder;
        }

        [Route("register")]
        [HttpPost]
        [ValidateModel]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            var registerInfo = _mapper.Map<RegisterInfo>(model);
            await _registrar.RegisterUser(registerInfo);
            return Ok();
        }

        [Authorize]
        [Route("getProfileInfo")]
        [HttpGet]
        public async Task<IActionResult> GetProfileInfo(Guid userId)
        {
            var user = await _profileFinder.FindById(userId);
            var response = _mapper.Map<UserResponse>(user);
            return Ok(response);
        }

        [Authorize]
        [Route("editProfile")]
        [HttpPost]
        public async Task<IActionResult> EditProfile(EditProfileModel model)
        {
            var userId = Guid.Parse(User.Claims.First(c => c.Type == AuthConstants.Claims.Id).Value);
            var profileInfo = _mapper.Map<ProfileInfo>(model);
            profileInfo.UserId = userId;

            var updatedUser = await _profileEditor.Edit(profileInfo);
            var response = _mapper.Map<UserResponse>(updatedUser);
            return Ok(response);
        }

        [Authorize]
        [Route("changeProfileImage")]
        [HttpPost]
        public async Task<IActionResult> ChangeProfileImage(IFormFile file)
        {
            var userId = Guid.Parse(User.Claims.First(c => c.Type == AuthConstants.Claims.Id).Value);

            var updatedUser = await _profileEditor.ChangeImage(file, userId);
            var response = _mapper.Map<UserResponse>(updatedUser);
            return Ok(response);
        }

        [Authorize]
        [Route("findProfiles")]
        [HttpGet]
        public async Task<IActionResult> FindProfiles([FromQuery] string name)
        {
            var userId = Guid.Parse(User.Claims.First(c => c.Type == AuthConstants.Claims.Id).Value);
            var profiles = await _profileFinder.FindByName(name, userId);
            var response = _mapper.Map<IEnumerable<UserResponse>>(profiles);
            return Ok(response);
        }

        [Authorize]
        [Route("addContact")]
        [HttpPost]
        public async Task<IActionResult> AddContact([FromQuery] Guid contactId)
        {
            var userId = Guid.Parse(User.Claims.First(c => c.Type == AuthConstants.Claims.Id).Value);
            await _contactAdder.Add(userId, contactId);

            return Ok();
        }
        
        [Authorize]
        [Route("getContacts")]
        [HttpGet]
        public async Task<IActionResult> GetContacts()
        {
            var userId = Guid.Parse(User.Claims.First(c => c.Type == AuthConstants.Claims.Id).Value);
            var contacts = await _contactsFinder.GetByUserId(userId);
            var response = _mapper.Map<IEnumerable<UserResponse>>(contacts);
                
            return Ok(response);
        }
    }
}
