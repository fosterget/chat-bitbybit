﻿using AutoMapper;
using Messanger.Core.Constants;
using Messanger.Core.Exceptions;
using Messanger.Core.Interfaces.Services.Auth;
using Messanger.Core.Models.Auth;
using Messanger.WebApp.Attributes;
using Messanger.WebApp.Helpers;
using Messanger.WebApp.Models.Auth;
using Microsoft.AspNetCore.Mvc;

namespace Messanger.WebApp.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthenticator _authenticator;

        private readonly ITokenRefresher _tokenRefresher;

        private readonly ITokenRevoker _tokenRevoker;

        private readonly IMapper _mapper;

        public AuthController(
            IAuthenticator authenticator,
            ITokenRefresher tokenRefresher,
            ITokenRevoker tokenRevoker,
            IMapper mapper)
        {
            _authenticator = authenticator;
            _tokenRefresher = tokenRefresher;
            _tokenRevoker = tokenRevoker;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [Route("authenticate")]
        [HttpPost]
        public async Task<IActionResult> Authenticate([FromBody] AuthenticateModel model)
        {
            var result = await _authenticator.AuthenticateAsync(new AuthInfo()
            {
                Email = model.Email,
                Password = model.Password,
                IpAddress = IpAddressHelper.GetIpFromRequest(this)
            });

            SetRefreshTokenCookie(result.RefreshToken);

            return Ok(_mapper.Map<AuthenticateResponse>(result));
        }

        [AllowAnonymous]
        [Route("refreshToken")]
        [HttpPost]
        public async Task<IActionResult> RefreshToken()
        {
            var refreshToken = Request.Cookies["refreshToken"];
            if (string.IsNullOrEmpty(refreshToken))
                throw new MissingArgumentException("Refresh token in cookies");

            var result = await _tokenRefresher.RefreshTokenAsync(refreshToken, IpAddressHelper.GetIpFromRequest(this));
            SetRefreshTokenCookie(result.RefreshToken);

            return Ok(result);
        }

        [Route("revokeToken")]
        [HttpPost]
        public async Task<IActionResult> RevokeToken([FromBody] RevokeTokenModel model)
        {
            var token = model.Token ?? Request.Cookies["refreshToken"];

            if (string.IsNullOrEmpty(token))
                throw new InvalidArgumentException("Token");

            await _tokenRevoker.RevokeTokenAsync(token, IpAddressHelper.GetIpFromRequest(this));
            return Ok(new { message = "Token revoked" });
        }

        private void SetRefreshTokenCookie(string token)
        {
            // Append cookie with refresh token to response
            var cookieOptions = new CookieOptions
            {
                HttpOnly = true,
                Expires = DateTime.UtcNow.AddDays(AuthConstants.RefreshTokenCookieLifetimeDays)
            };
            Response.Cookies.Append("refreshToken", token, cookieOptions);
        }
    }
}
