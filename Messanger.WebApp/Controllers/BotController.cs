﻿using AutoMapper;
using Messanger.Core.Constants;
using Messanger.Core.Enums;
using Messanger.Core.Interfaces.Services;
using Messanger.Core.Interfaces.Services.Bots;
using Messanger.Core.Models.Bots;
using Messanger.WebApp.Attributes;
using Messanger.WebApp.Models.Account;
using Messanger.WebApp.Models.Bot;
using Messanger.WebApp.Models.Chat;
using Microsoft.AspNetCore.Mvc;

namespace Messanger.WebApp.Controllers;


[Route("api/[controller]")]
[ApiController]
public class BotController : ControllerBase
{
    private readonly IMapper _mapper;
    
    private readonly IBotCreator _botCreator;

    private readonly IBotFinder _botFinder;

    private readonly IBotMessenger _botMessenger;

    private readonly IResultsIssuer _resultsIssuer;

    public BotController(IMapper mapper, IBotCreator botCreator, IBotFinder botFinder, IBotMessenger botMessenger, IResultsIssuer resultsIssuer)
    {
        _mapper = mapper;
        _botCreator = botCreator;
        _botFinder = botFinder;
        _botMessenger = botMessenger;
        _resultsIssuer = resultsIssuer;
    }
    
    [Authorize]
    [Route("create")]
    [HttpPost]
    public async Task<IActionResult> Create([FromBody] BotModel model)
    {
        var userId = Guid.Parse(User.Claims.First(c => c.Type == AuthConstants.Claims.Id).Value);
        var steps = _mapper.Map<IEnumerable<StepInfo>>(model.Steps).ToList();
            
        await _botCreator.Create(userId, model.Name, steps);
        return Ok();
    }

    [Authorize]
    [Route("SendMessage")]
    [HttpPost]
    public async Task<IActionResult> SendMessage(Guid botId, string message)
    {
        var userId = Guid.Parse(User.Claims.First(c => c.Type == AuthConstants.Claims.Id).Value);

        var messages = await _botMessenger.SendMessageAsync(userId, botId, message);
        var response = _mapper.Map<List<MessageResponse>>(messages);
        
        return Ok(response);
    }

    [Authorize]
    [Route("FindBots")]
    [HttpGet]
    public async Task<IActionResult> FindBots(string query)
    {
        var bots = await _botFinder.FindByName(query);
        var response = _mapper.Map<IEnumerable<InterlocutorResponse>>(bots);
        return Ok(response);
    }
    
    [Authorize]
    [Route("FindBotsByCreator")]
    [HttpGet]
    public async Task<IActionResult> FindBotsByCreator()
    {
        var userId = Guid.Parse(User.Claims.First(c => c.Type == AuthConstants.Claims.Id).Value);

        var bots = await _botFinder.FindByCreator(userId);
        var response = _mapper.Map<IEnumerable<InterlocutorResponse>>(bots);
        return Ok(response);
    }
    
    [Authorize]
    [Route("DownloadResults")]
    [HttpGet]
    public async Task<IActionResult> DownloadResults(Guid botId)
    {
        var results = await _resultsIssuer.GetResults(botId);

        return File(results.Buffer, results.ContentType, results.Name);
    }
}