using AutoMapper;
using Messanger.Core.Constants;
using Messanger.Core.Interfaces.Services.Team;
using Messanger.WebApp.Attributes;
using Messanger.WebApp.Models.Account;
using Messanger.WebApp.Models.Team;
using Microsoft.AspNetCore.Mvc;

namespace Messanger.WebApp.Controllers;

[Authorize]
[Route("api/[controller]")]
[ApiController]
public class TeamController : ControllerBase
{
    private readonly IMapper _mapper;
    
    private readonly ITeamCreator _teamCreator;

    private readonly ITeamUsersManager _teamUsers;

    private readonly ITeamsFinder _teamsFinder;
    
    public TeamController(IMapper mapper, ITeamCreator teamCreator, ITeamUsersManager teamUsers, ITeamsFinder teamsFinder)
    {
        _mapper = mapper;
        _teamCreator = teamCreator;
        _teamUsers = teamUsers;
        _teamsFinder = teamsFinder;
    }

    [Route("CreateTeam")]
    [HttpPost]
    public async Task<IActionResult> CreateTeam([FromBody] TeamModel teamModel)
    {
        var userId = Guid.Parse(User.Claims.First(c => c.Type == AuthConstants.Claims.Id).Value);
        await _teamCreator.Create(userId, teamModel.Name, teamModel.UserIds);
        return Ok();
    }

    [Route("AddUserToTeam")]
    [HttpPost]
    public async Task<IActionResult> AddUserToTeam([FromBody] TeamUserModel model)
    {
        await _teamUsers.AddUser(new Guid(model.TeamId), new Guid(model.UserId));
        return Ok(model.UserId);
    }
    
    [Route("RemoveUserFromTeam")]
    [HttpPost]
    public async Task<IActionResult> RemoveUserFromTeam([FromBody] TeamUserModel model)
    {
        await _teamUsers.DeleteUser(new Guid(model.TeamId), new Guid(model.UserId));
        return Ok(model.UserId);
    }

    [Route("GetCreatedTeams")]
    [HttpGet]
    public async Task<IActionResult> GetCreatedTeams()
    {
        var userId = Guid.Parse(User.Claims.First(c => c.Type == AuthConstants.Claims.Id).Value);
        var teams = await _teamsFinder.GetTeamsCreatedByUserAsync(userId);
        return Ok(_mapper.Map<IEnumerable<TeamResponse>>(teams));
    }

    [Route("GetTeamUsers")]
    [HttpGet]
    public async Task<IActionResult> GetTeamUsers([FromQuery] Guid teamId)
    {
        var users = await _teamsFinder.GetTeamUsersAsync(teamId);
        return Ok(_mapper.Map<IEnumerable<UserResponse>>(users));
    }
}