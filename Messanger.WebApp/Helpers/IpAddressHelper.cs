﻿using Microsoft.AspNetCore.Mvc;

namespace Messanger.WebApp.Helpers
{
    public static class IpAddressHelper
    {
        public static string GetIpFromRequest(ControllerBase controller)
        {
            // get source ip address for the current request
            if (controller.Request.Headers.ContainsKey("X-Forwarded-For"))
                return controller.Request.Headers["X-Forwarded-For"];
            else
                return controller.HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }
    }
}
