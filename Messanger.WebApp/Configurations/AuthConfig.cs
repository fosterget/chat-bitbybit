﻿using Messanger.Core.Interfaces.Services.Auth;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Options;

namespace Messanger.WebApp.Configurations
{
    public static class AuthConfig
    {
        public static IServiceCollection AddAuthenticationConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IPostConfigureOptions<JwtBearerOptions>, ConfigureJwtBearerOptions>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    //options.Authority = "Authority URL"; // TODO: Update URL
                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            var accessToken = context.Request.Query["access_token"];

                            // If the request is for our hub...
                            var path = context.HttpContext.Request.Path;
                            if (!string.IsNullOrEmpty(accessToken) &&
                                (path.StartsWithSegments("/chatHub")))
                            {
                                // Read the token out of the query string
                                context.Token = accessToken;
                            }
                            return Task.CompletedTask;
                        }
                    };
                });

            return services;
        }

        private class ConfigureJwtBearerOptions : IPostConfigureOptions<JwtBearerOptions>
        {
            private readonly IAccessTokenValidator _accessTokenValidator;

            public ConfigureJwtBearerOptions(IAccessTokenValidator accessTokenValidator)
            {
                _accessTokenValidator = accessTokenValidator;
            }

            public void PostConfigure(string name, JwtBearerOptions options)
            {
                _accessTokenValidator.ApplyValidationOptions(options);
            }
        }
    }
}
