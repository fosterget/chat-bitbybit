﻿using AutoMapper;
using Messanger.WebApp.Configurations.MappingProfiles;

namespace Messanger.WebApp.Configurations
{
    public static class AutoMapperConfiguration
    {
        public static IServiceCollection AddMapper(this IServiceCollection services)
        {
            var config = new MapperConfiguration(m =>
            {
                m.AddProfile(new AccountMappingProfile());
                m.AddProfile(new AuthenticateMappingProfile());
                m.AddProfile(new ChatMappingProfile());
                m.AddProfile(new BotMappingProfile());
                m.AddProfile(new TeamMappingProfile());
            });

            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            return services;
        }
    }
}
