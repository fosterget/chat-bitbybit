﻿using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Repositories;
using Messanger.Core.Interfaces.Services;
using Messanger.Core.Interfaces.Services.Account;
using Messanger.Core.Interfaces.Services.Auth;
using Messanger.Core.Interfaces.Services.Bots;
using Messanger.Core.Interfaces.Services.Chat;
using Messanger.Core.Interfaces.Services.Team;
using Messanger.Core.Services.Account;
using Messanger.Core.Services.Auth;
using Messanger.Core.Services.Bots;
using Messanger.Core.Services.Chat;
using Messanger.Core.Services.Team;
using Messanger.Infrastructure;
using Messanger.Infrastructure.Repositories;

namespace Messanger.WebApp.Configurations
{
    public static class DIConfig
    {
        public static IServiceCollection AddAppServices(this IServiceCollection services)
        {
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IDialogRepository, DialogRepository>();
            services.AddTransient<IMessageRepository, MessageRepositroy>();
            services.AddTransient<IBotRepository, BotRepository>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            services.AddTransient<IRegistrar, Registrar>();
            services.AddTransient<IAuthenticator, Authenticator>();
            services.AddTransient<ITokenRefresher, TokenRefresher>();
            services.AddTransient<ITokenRevoker, TokenRevoker>();
            services.AddTransient<IDialogsFinder, DialogsFinder>();
            services.AddTransient<IDialogCreator, DialogCreator>();
            services.AddTransient<IMessageSender, MessageSender>();
            services.AddTransient<IMessageFinder, MessageFinder>();
            services.AddTransient<IProfileEditor, ProfileEditor>();
            services.AddTransient<IProfileFinder, ProfileFinder>();
            services.AddTransient<IContactAdder, ContactAdder>();
            services.AddTransient<IContactsFinder, ContactsFinder>();
            services.AddTransient<IBotCreator, BotCreator>();
            services.AddTransient<IBotFinder, BotFinder>();
            services.AddTransient<IBotMessenger, BotMessenger>();
            services.AddTransient<IResultsIssuer, ResultsIssuer>();
            services.AddTransient<ITeamCreator, TeamCreator>();
            services.AddTransient<ITeamUsersManager, TeamUsersManager>();
            services.AddTransient<ITeamsFinder, TeamsFinder>();

            services.AddTransient<IAccessTokenGenerator, AccessTokenGenerator>();
            services.AddTransient<IAccessTokenValidator, AccessTokenValidator>();
            services.AddTransient<IRefreshTokenGenerator, RefreshTokenGenerator>();

            return services;
        }
    }
}
