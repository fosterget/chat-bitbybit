﻿using AutoMapper;
using Messanger.Core.Models.Auth;
using Messanger.WebApp.Models.Auth;

namespace Messanger.WebApp.Configurations.MappingProfiles
{
    public class AuthenticateMappingProfile : Profile
    {
        public AuthenticateMappingProfile()
        {
            CreateMap<AuthenticateModel, AuthInfo>();
            CreateMap<AuthResult, AuthenticateResponse>();
        }
    }
}
