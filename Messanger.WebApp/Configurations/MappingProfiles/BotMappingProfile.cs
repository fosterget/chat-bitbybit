﻿using AutoMapper;
using Messanger.Core.Models.Bots;
using Messanger.WebApp.Models.Bot;

namespace Messanger.WebApp.Configurations.MappingProfiles;

public class BotMappingProfile : Profile
{
    public BotMappingProfile()
    {
        CreateMap<StepModel, StepInfo>();
        CreateMap<ButtonModel, ButtonInfo>();
    }
}