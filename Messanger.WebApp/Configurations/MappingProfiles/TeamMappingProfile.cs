﻿using AutoMapper;
using Messanger.Core.Entities;
using Messanger.WebApp.Models.Team;

namespace Messanger.WebApp.Configurations.MappingProfiles;

public class TeamMappingProfile : Profile
{
    public TeamMappingProfile()
    {
        CreateMap<Team, TeamResponse>();
    }
}