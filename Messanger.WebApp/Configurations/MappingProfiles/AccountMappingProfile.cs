﻿using AutoMapper;
using Messanger.Core.Entities;
using Messanger.Core.Models.Account;
using Messanger.WebApp.Models.Account;

namespace Messanger.WebApp.Configurations.MappingProfiles
{
    public class AccountMappingProfile : Profile
    {
        public AccountMappingProfile()
        {
            CreateMap<RegisterModel, RegisterInfo>();
            CreateMap<EditProfileModel, ProfileInfo>();
            CreateMap<User, UserResponse>();
            CreateMap<Interlocutor, InterlocutorResponse>();
        }
    }
}
