﻿using AutoMapper;
using Messanger.Core.Entities;
using Messanger.Core.Models.Chat;
using Messanger.WebApp.Models.Chat;
using File = Messanger.Core.Entities.File;

namespace Messanger.WebApp.Configurations.MappingProfiles
{
    public class ChatMappingProfile : Profile
    {
        public ChatMappingProfile()
        {
            CreateMap<MessageModel, MessageInfo>();
            CreateMap<Dialog, UserDialogResponse>();
            CreateMap<Dialog, BotDialogResponse>()
                .ForMember(dest => dest.DialogType, opt => opt.MapFrom(src => src.Type));
            CreateMap<Dialog, GroupDialogResponse>();
            CreateMap<Message, MessageResponse>();
            CreateMap<MessageButton, ButtonResponse>();
            CreateMap<File, FileResponse>();
        }
    }
}
