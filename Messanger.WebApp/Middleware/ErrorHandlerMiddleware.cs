using System.Net;
using System.Text.Json;
using Messanger.Core.Exceptions;
using FileNotFoundException = Messanger.Core.Exceptions.FileNotFoundException;

namespace Messanger.WebApp.Middleware;

public class ErrorHandlerMiddleware
{
    private readonly RequestDelegate _next;

    public ErrorHandlerMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(HttpContext context)
    {
        try
        {
            await _next(context);
        }
        catch (Exception error)
        {
            var response = context.Response;
            response.ContentType = "application/json";

            switch (error)
            {
                case InvalidArgumentException:
                    response.StatusCode = InvalidArgumentException.StatusCode;
                    break;
                case AuthException:
                    response.StatusCode = AuthException.StatusCode;
                    break;
                case AlreadyExistsException:
                    response.StatusCode = AlreadyExistsException.StatusCode;
                    break;
                case MissingArgumentException:
                    response.StatusCode = MissingArgumentException.StatusCode;
                    break;
                case FileNotFoundException:
                    response.StatusCode = FileNotFoundException.StatusCode;
                    break;
                default:
                    response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    break;
            }

            var result = JsonSerializer.Serialize(new {message = error.Message});
            await response.WriteAsync(result);
        }
    }
}