﻿using Messanger.Core.Constants;
using Messanger.Core.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Messanger.WebApp.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            // Skip authorization if action has [AllowAnonymous] attribute
            var hasAllowAnonymous = context.ActionDescriptor.EndpointMetadata.OfType<AllowAnonymousAttribute>().Any();
            if (hasAllowAnonymous) 
                return;

            // Authorize
            var userExists = context.HttpContext.User.Claims.Any(c => c.Type == AuthConstants.Claims.Id);
            if (!userExists)
                context.Result = new JsonResult(new { message = "Unauthorized"}) { StatusCode = StatusCodes.Status401Unauthorized };
        }
    }
}
