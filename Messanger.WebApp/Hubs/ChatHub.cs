﻿using System.Runtime.InteropServices.ComTypes;
using AutoMapper;
using Messanger.Core.Entities;
using Messanger.Core.Exceptions;
using Messanger.Core.Interfaces.Services.Bots;
using Messanger.Core.Interfaces.Services.Chat;
using Messanger.Core.Models.Chat;
using Messanger.WebApp.Models.Chat;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace Messanger.WebApp.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {
        private readonly IMapper _mapper;

        private readonly IMessageSender _messageSender;

        private readonly IBotMessenger _botMessenger;

        public ChatHub(IMapper mapper, IMessageSender messageSender, IBotMessenger botMessenger)
        {
            _mapper = mapper;
            _messageSender = messageSender;
            _botMessenger = botMessenger;
        }
        
        public override Task OnConnectedAsync()
        {
            var userId = Guid.Parse(Context.UserIdentifier ?? throw new AuthException("Invalid token or claims"));
            return base.OnConnectedAsync();
        }

        [Authorize]
        public async Task SendMessage(MessageModel model)
        {
            var userId = Guid.Parse(Context.UserIdentifier ?? throw new AuthException("Invalid token or claims"));
            var messageInfo = _mapper.Map<MessageInfo>(model);
            messageInfo.AuthorId = userId;

            if (model.DialogId == default)
                throw new InvalidArgumentException($"{nameof(model.DialogId)} should be initialized");
            
            var message = await _messageSender.SendMessageByDialogAsync(messageInfo);
            var response = _mapper.Map<MessageResponse>(message);
            foreach (var user in message.Dialog.Interlocutors)
            {
                await Clients.User(user.Id.ToString()).SendAsync("ReceiveMessage", response);
            }
        }

        [Authorize]
        public async Task SendBotMessage(Guid botId, string message)
        {
            var userId = Guid.Parse(Context.UserIdentifier ?? throw new AuthException("Invalid token or claims"));

            var messages = await _botMessenger.SendMessageAsync(userId, botId, message);
            var response = _mapper.Map<List<MessageResponse>>(messages);
            
            foreach (var msg in response)
            {
                await Clients.User(userId.ToString()).SendAsync("ReceiveMessage", msg);
            }
        }
    }
}
