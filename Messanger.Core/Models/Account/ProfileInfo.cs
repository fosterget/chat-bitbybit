﻿namespace Messanger.Core.Models.Account;

public class ProfileInfo
{
    public Guid UserId { get; set; }
    
    public string Name { get; set; }
        
    public string Surname { get; set; }
    
    public string Position { get; set; }
    
    public string Bio { get; set; }
        
    public string Email { get; set; }
}