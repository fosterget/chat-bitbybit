﻿namespace Messanger.Core.Models.Account
{
    public class RegisterInfo
    {
        public string Name { get; set; }
        
        public string Surname { get; set; }
        
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
