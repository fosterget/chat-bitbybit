﻿using Messanger.Core.Enums;
using Microsoft.AspNetCore.Http;

namespace Messanger.Core.Models.Chat
{
    public class MessageInfo
    {
        public string Text { get; set; }

        public Guid DialogId { get; set; }

        public Guid AuthorId { get; set; }

        public Guid RecipientId { get; set; }
        
        public IFormFile? File { get; set; }
        
        public FileType? FileType { get; set; }
        
        public List<string>? ButtonTitles { get; set; }
    }
}
