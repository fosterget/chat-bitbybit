﻿namespace Messanger.Core.Models.Chat
{
    public class PaginationInfo
    {
        public int Offset { get; set; }

        public int Limit { get; set; }

        public PaginationInfo(int offset, int limit)
        {
            Offset = offset;
            Limit = limit;
        }
    }
}
