namespace Messanger.Core.Models.Common;

public class FileDTO
{
    public Stream Content { get; set; }
    
    public string ContentType { get; set; }
}