﻿using Messanger.Core.Enums;

namespace Messanger.Core.Models.Bots;

public class StepInfo
{
    public int Id;

    public string Name;
    
    public string Text;
    
    public bool SaveData = true;
    
    public AnswerType AnswerType;

    public int? NextStepId;

    public IEnumerable<ButtonInfo>? Buttons;
}