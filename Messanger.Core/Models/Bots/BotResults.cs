﻿namespace Messanger.Core.Models.Bots;

public class BotResults
{
    public string Name;

    public string ContentType;

    public byte[] Buffer;
}