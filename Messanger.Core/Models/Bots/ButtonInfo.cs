﻿namespace Messanger.Core.Models.Bots;

public class ButtonInfo
{
    public string Title;

    public int? NextStepId;
}