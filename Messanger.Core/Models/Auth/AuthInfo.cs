﻿namespace Messanger.Core.Models.Auth
{
    public class AuthInfo
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string IpAddress { get; set; }
    }
}
