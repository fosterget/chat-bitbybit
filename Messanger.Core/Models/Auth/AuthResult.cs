﻿namespace Messanger.Core.Models.Auth
{
    public class AuthResult
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        
        public string Surname { get; set; }
        
        public string Email { get; set; }
        
        public DateTime ProfileImageUpdated { get; set; }

        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }
    }
}
