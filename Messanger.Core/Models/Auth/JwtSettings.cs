﻿namespace Messanger.Core.Models.Auth
{
    public class JwtSettings
    {
        public string Secret { get; set; }

        // refresh token time to live in days
        public int RefreshTokenTTL { get; set; }
    }
}
