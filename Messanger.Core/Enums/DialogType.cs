﻿namespace Messanger.Core.Enums;

public enum DialogType
{
    Personal,
    Group,
    Bot
}