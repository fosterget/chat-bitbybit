﻿namespace Messanger.Core.Enums;

public enum InterlocutorType
{
    User,
    Bot
}