﻿namespace Messanger.Core.Enums;

public enum AnswerType
{
    None,
    Text,
    Button
}