namespace Messanger.Core.Constants;

public static class AccountConstants
{
    public const int NameMaxLength = 15;
    public const int NameMinLength = 1;
    
    public const int SurnameMaxLength = 20;
    public const int SurnameMinLength = 1;
    
    public const int PasswordMinLength = 6;
    public const int PasswordMaxLength = 16;

    public const int BioMaxLength = 200;
    public const int BioMinLength = 1;

    public const int PositionMaxLength = 16;
    public const int PositionMinLength = 1;
}