namespace Messanger.Core.Constants;

public class AuthConstants
{
    public const int AccessTokenLifetimeDays = 100;
    public const int RefreshTokenLifetimeDays = 200;
    public const int RefreshTokenCookieLifetimeDays = 200;

    public class Claims
    {
        public const string Id = "Id";
    }
}