﻿using Messanger.Core.Entities;

namespace Messanger.Core.Interfaces.Repositories;

public interface IStepRepository : IGenericRepository<Step>
{
    Task<Step?> GetByIdWithSteps(Guid id);

    Task<Step?> GetByBotIdWithSteps(Guid id);
}