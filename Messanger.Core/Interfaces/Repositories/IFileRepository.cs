﻿using File = Messanger.Core.Entities.File;

namespace Messanger.Core.Interfaces.Repositories;

public interface IFileRepository : IGenericRepository<File>
{
    
}