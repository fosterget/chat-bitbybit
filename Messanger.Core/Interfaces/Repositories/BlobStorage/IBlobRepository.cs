using Messanger.Core.Models.Common;
using Microsoft.AspNetCore.Http;

namespace Messanger.Core.Interfaces.Repositories.BlobStorage;

public interface IBlobRepository
{
    Task<FileDTO> GetFile(string name);
    
    Task<string> AddFile(IFormFile file, string filename);
}