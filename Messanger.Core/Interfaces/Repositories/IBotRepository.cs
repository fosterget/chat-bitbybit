﻿using Messanger.Core.Entities;

namespace Messanger.Core.Interfaces.Repositories;

public interface IBotRepository : IGenericRepository<Bot>
{
    Task<Bot?> GetBotByIdAsync(Guid botId);
}