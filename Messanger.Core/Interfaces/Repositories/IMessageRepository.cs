﻿using Messanger.Core.Entities;

namespace Messanger.Core.Interfaces.Repositories
{
    public interface IMessageRepository : IGenericRepository<Message>
    {
        Task<IEnumerable<Message>> GetByDialogId(Guid dialogId, int offset, int limit);
    }
}
