﻿using Messanger.Core.Entities;

namespace Messanger.Core.Interfaces.Repositories;

public interface IStepButtonRepository : IGenericRepository<StepButton>
{
    
}