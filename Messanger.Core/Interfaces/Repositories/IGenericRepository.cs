﻿using System.Linq.Expressions;

namespace Messanger.Core.Interfaces.Repositories
{
    public interface IGenericRepository<T>
    {
        Task<T?> GetByIdAsync(Guid id);

        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);

        Task<IEnumerable<T>> GetWhereAsync(Expression<Func<T, bool>> predicate);

        Task<T?> GetWhereSingleAsync(Expression<Func<T, bool>> predicate);

        Task<int> CountByPredicateAsync(Expression<Func<T, bool>> predicate);
    }
}
