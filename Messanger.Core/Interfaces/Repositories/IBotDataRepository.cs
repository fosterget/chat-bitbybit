﻿namespace Messanger.Core.Interfaces.Repositories;

public interface IBotDataRepository
{
    Task CreateBotAsync(Guid botId, Guid userId);

    Task<Dictionary<string, string>> GetUserDataAsync(Guid botId, Guid userId);

    Task SaveUserDataAsync(Guid botId, Guid userId, Dictionary<string, string> data);

    Task<List<Dictionary<string, string>>> GetBotResults(Guid botId);
}