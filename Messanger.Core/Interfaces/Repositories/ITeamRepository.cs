using Messanger.Core.Entities;

namespace Messanger.Core.Interfaces.Repositories;

public interface ITeamRepository : IGenericRepository<Team>
{
    Task<Team> GetTeamWithUsersAsync(Guid teamId);

    Task<IEnumerable<Team>> GetTeamsByOwnerAsync(Guid ownerId);

    Task<IEnumerable<User>> GetTeamUsers(Guid teamId);
}