﻿using Messanger.Core.Entities;

namespace Messanger.Core.Interfaces.Repositories
{
    public interface IUserRepository : IGenericRepository<User>
    {
        Task<User?> FindByEmailAsync(string email);

        Task<bool> IsTokenUniqueAsync(string token);

        Task<User?> GetUserByRefreshTokenAsync(string token);

        Task<User?> GetUserWithDialogsByIdAsync(Guid userId);

        Task<IEnumerable<User>> FindByNameAndSurname(string query, Guid userIdToExclude);

        Task<User?> GetUserWithContacts(Guid userId);

        Task<IEnumerable<User>> GetUserContacts(Guid userId);
    }
}
