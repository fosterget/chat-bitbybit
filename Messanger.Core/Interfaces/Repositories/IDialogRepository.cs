﻿using Messanger.Core.Entities;
using Messanger.Core.Enums;

namespace Messanger.Core.Interfaces.Repositories
{
    public interface IDialogRepository : IGenericRepository<Dialog>
    {
        Task<IEnumerable<Dialog>> GetByUser(User user, DialogType? type = null);

        Task<IEnumerable<Dialog>> GetByUserId(Guid userId);

        Task<Dialog?> GetWithMessagesAndUsers(Guid dialogId);
    }
}
