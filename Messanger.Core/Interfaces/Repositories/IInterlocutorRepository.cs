﻿using Messanger.Core.Entities;

namespace Messanger.Core.Interfaces.Repositories;

public interface IInterlocutorRepository : IGenericRepository<Interlocutor>
{
    
}