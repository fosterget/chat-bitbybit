﻿using Messanger.Core.Interfaces.Repositories;

namespace Messanger.Core.Interfaces;

public interface IUnitOfWork
{
    IDialogRepository Dialogs { get; }
    
    IMessageRepository Messages { get; }
    
    IInterlocutorRepository Interlocutors { get; }
    
    IUserRepository Users { get; }
    
    IBotRepository Bots { get; }
    
    IStepRepository Steps { get; }
    
    ITeamRepository Teams { get; }
    
    IFileRepository Files { get; }
    
    Task<int> Complete();
    
}