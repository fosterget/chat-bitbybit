﻿using Messanger.Core.Models.Bots;

namespace Messanger.Core.Interfaces;

public interface IBotResultsCsvGenerator
{
    Task<BotResults> GetResults(Guid botId);
}