﻿using Messanger.Core.Entities;
using Messanger.Core.Enums;

namespace Messanger.Core.Interfaces.Services.Chat
{
    public interface IDialogsFinder
    {
        Task<IEnumerable<Dialog>> GetPersonalDialogsAsync(Guid userId, DialogType type);
    }
}
