﻿using Messanger.Core.Entities;
using Messanger.Core.Models.Chat;

namespace Messanger.Core.Interfaces.Services.Chat;

public interface IMessageFinder
{
    Task<IEnumerable<Message>> GetMessagesAsync(Guid userId, Guid dialogId, PaginationInfo pagination);
}