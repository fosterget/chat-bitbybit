﻿using Messanger.Core.Entities;
using Messanger.Core.Models.Chat;
using Microsoft.AspNetCore.Http;

namespace Messanger.Core.Interfaces.Services.Chat;

public interface IMessageSender
{
    Task<Message> SendMessageByDialogAsync(MessageInfo messageInfo);
    
    Task<Message> SendMessageByRecipientAsync(MessageInfo messageInfo);

    Task<Message> SendImage(Guid userId, Guid dialogId, IFormFile file);
}