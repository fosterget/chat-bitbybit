﻿using Messanger.Core.Entities;
using Messanger.Core.Enums;

namespace Messanger.Core.Interfaces.Services.Chat;

public interface IDialogCreator
{
    /// <summary>
    /// Creates Dialog Entity, but does not save it by SaveChanges() EF method.
    /// Required to call SaveChanges() in caller method.
    /// </summary>
    /// <param name="userIds">List of dialog participants ids</param>
    /// <returns></returns>
    Task<Dialog> CreateDialogAsync(IEnumerable<Guid> userIds, DialogType? dialogType = null);
}