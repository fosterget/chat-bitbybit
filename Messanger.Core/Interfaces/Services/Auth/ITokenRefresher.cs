﻿using Messanger.Core.Models.Auth;

namespace Messanger.Core.Interfaces.Services.Auth
{
    public interface ITokenRefresher
    {
        Task<AuthResult> RefreshTokenAsync(string token, string ipAddress);
    }
}
