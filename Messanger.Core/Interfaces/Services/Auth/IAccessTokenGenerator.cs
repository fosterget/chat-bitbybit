﻿namespace Messanger.Core.Interfaces.Services.Auth
{
    public interface IAccessTokenGenerator
    {
        string GenerateToken(Guid id);
    }
}
