﻿using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Messanger.Core.Interfaces.Services.Auth
{
    public interface IAccessTokenValidator
    {
        /// <summary>
        /// Method for validating token
        /// </summary>
        /// <param name="token">JWT token to validate</param>
        /// <returns>User id. Null if token is invalid.</returns>
        void ApplyValidationOptions(JwtBearerOptions options);
    }
}
