﻿using Messanger.Core.Entities;

namespace Messanger.Core.Interfaces.Services.Auth
{
    public interface IRefreshTokenGenerator
    {
        Task<RefreshToken> GenerateTokenAsync(string ipAddress);
    }
}
