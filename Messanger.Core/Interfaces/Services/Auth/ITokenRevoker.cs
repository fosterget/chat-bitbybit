﻿namespace Messanger.Core.Interfaces.Services.Auth
{
    public interface ITokenRevoker
    {
        Task RevokeTokenAsync(string token, string ipAddress);
    }
}
