﻿using Messanger.Core.Models.Auth;

namespace Messanger.Core.Interfaces.Services.Auth
{
    public interface IAuthenticator
    {
        Task<AuthResult> AuthenticateAsync(AuthInfo authInfo);
    }
}
