﻿using Messanger.Core.Entities;

namespace Messanger.Core.Interfaces.Services.Bots;

public interface IBotFinder
{
    public Task<Bot> FindById(Guid id);

    public Task<IEnumerable<Bot>> FindByCreator(Guid id);
    
    public Task<IEnumerable<Bot>> FindByName(string name);
}