﻿using Messanger.Core.Entities;

namespace Messanger.Core.Interfaces.Services.Bots;

public interface IBotMessenger
{
    Task<List<Message>> SendMessageAsync(Guid userId, Guid botId, string messageText);
}