﻿using Messanger.Core.Models.Bots;

namespace Messanger.Core.Interfaces.Services.Bots;

public interface IBotCreator
{
    public Task Create(Guid creatorId, string botName, List<StepInfo> stepsInfo);
}