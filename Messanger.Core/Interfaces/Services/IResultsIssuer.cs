﻿using Messanger.Core.Models.Bots;

namespace Messanger.Core.Interfaces.Services;

public interface IResultsIssuer
{
    Task<BotResults> GetResults(Guid botId);
}