namespace Messanger.Core.Interfaces.Services.Team;

public interface ITeamUsersManager
{
    Task AddUser(Guid teamId, Guid userId);
    
    Task DeleteUser(Guid teamId, Guid userId);
}