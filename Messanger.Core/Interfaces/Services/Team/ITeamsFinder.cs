﻿using Messanger.Core.Entities;

namespace Messanger.Core.Interfaces.Services.Team;

public interface ITeamsFinder
{
    Task<IEnumerable<Entities.Team>> GetTeamsCreatedByUserAsync(Guid userId);

    Task<IEnumerable<User>> GetTeamUsersAsync(Guid teamId);
}