using Messanger.Core.Entities;

namespace Messanger.Core.Interfaces.Services.Team;

public interface ITeamCreator
{
    Task<Entities.Team> Create(Guid creatorId, string name, List<string> userIds);
}