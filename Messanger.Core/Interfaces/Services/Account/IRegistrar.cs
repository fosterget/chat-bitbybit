﻿using Messanger.Core.Models.Account;

namespace Messanger.Core.Interfaces.Services.Account
{
    public interface IRegistrar
    {
        Task RegisterUser(RegisterInfo registerInfo);
    }
}
