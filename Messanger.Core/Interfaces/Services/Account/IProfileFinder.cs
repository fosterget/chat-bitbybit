﻿using Messanger.Core.Entities;
using Messanger.Core.Models.Common;

namespace Messanger.Core.Interfaces.Services.Account;

public interface IProfileFinder
{
    Task<User> FindById(Guid userId);

    Task<IEnumerable<User>> FindByName(string query, Guid userId);

    Task<FileDTO> GetUserProfileImage(Guid userId);
}