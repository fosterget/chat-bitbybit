﻿using Messanger.Core.Entities;
using Messanger.Core.Models.Account;
using Microsoft.AspNetCore.Http;

namespace Messanger.Core.Interfaces.Services.Account;

public interface IProfileEditor
{
    Task<User> Edit(ProfileInfo profileInfo);

    Task<User> ChangeImage(IFormFile file, Guid userId);
}