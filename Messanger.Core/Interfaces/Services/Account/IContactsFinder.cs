﻿using Messanger.Core.Entities;

namespace Messanger.Core.Interfaces.Services.Account;

public interface IContactsFinder
{
    Task<IEnumerable<User>> GetByUserId(Guid userId);
}