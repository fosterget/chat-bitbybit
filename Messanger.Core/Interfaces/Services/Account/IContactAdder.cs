﻿using Messanger.Core.Entities;

namespace Messanger.Core.Interfaces.Services.Account;

public interface IContactAdder
{
    Task<User> Add(Guid userId, Guid contactId);
}