namespace Messanger.Core.Entities;

public class Team
{
    public Guid Id { get; set; }

    public string Name { get; set; }
    
    public Guid? OwnerId { get; set; }
    
    public User? Owner { get; set; }
    
    public Guid DialogId { get; set; }
    
    public Dialog Dialog { get; set; }

    public ICollection<User> Users { get; set; }
}