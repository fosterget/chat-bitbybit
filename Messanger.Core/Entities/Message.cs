﻿namespace Messanger.Core.Entities
{
    public class Message
    {
        public Guid Id { get; set; }

        public string? Text { get; set; }

        public DateTime Date { get; set; }

        public Guid AuthorId { get; set; }

        public Interlocutor Author { get; set; }

        public Guid DialogId { get; set; }

        public Dialog Dialog { get; set; }
        
        public Guid? FileId { get; set; }
        
        public File? File { get; set; }
        
        public ICollection<MessageButton> Buttons { get; set; }
    }
}
