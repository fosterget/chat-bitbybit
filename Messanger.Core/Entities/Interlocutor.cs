﻿using Messanger.Core.Enums;

namespace Messanger.Core.Entities;

public class Interlocutor
{
    public Guid Id { get; set; }

    public string Name { get; set; }
    
    public DateTime CreationDate { get; init; }

    public DateTime ProfileImageUpdated { get; set; }
    
    public string? ProfileImageUrl { get; set; }
    
    public InterlocutorType Type { get; set; }
    
    public ICollection<Dialog> Dialogs { get; set; }

    public ICollection<Message> Messages { get; set; }
}