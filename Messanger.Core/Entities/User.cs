﻿namespace Messanger.Core.Entities
{
    public class User : Interlocutor
    {
        public string Surname { get; set; }
        
        public string? Position { get; set; }
        
        public string? Bio { get; set; }
        
        public string Email { get; set; }
        
        public string Password { get; set; }

        public ICollection<RefreshToken> RefreshTokens { get; set; }

        public ICollection<Bot> Bots { get; set; }

        public ICollection<User> Contacts { get; set; }
        
        public ICollection<User> InContacts { get; set; }
        
        public ICollection<Team> Teams { get; set; }
        
        public ICollection<Team> CreatedTeams { get; set; }
    }
}