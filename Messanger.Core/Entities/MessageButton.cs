﻿namespace Messanger.Core.Entities;

public class MessageButton
{
    public Guid Id { get; set; }
    
    public string Title { get; set; }
    
    public Guid MessageId { get; set; }
    
    public Message Message { get; set; }
}