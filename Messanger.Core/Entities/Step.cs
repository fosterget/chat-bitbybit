﻿using Messanger.Core.Enums;

namespace Messanger.Core.Entities;

public class Step
{
    public Guid Id { get; set; }
    
    public Bot Bot { get; set; }
    
    public Guid BotId { get; set; }
    
    public string Name { get; set; }
    
    public string Text { get; set; }
    
    public bool SaveData { get; set; }
    
    public AnswerType AnswerType { get; set; }
    
    public Step? NextStep { get; set; }
    
    public ICollection<StepButton>? Buttons { get; set; }
}