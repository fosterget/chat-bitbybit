﻿namespace Messanger.Core.Entities;

public class StepButton
{
    public Guid Id { get; set; }
    
    public Step Step { get; set; }
    
    public Guid StepId { get; set; }
    
    public Step NextStep { get; set; }

    public Guid? NextStepId { get; set; }
    
    public string Title { get; set; }
}