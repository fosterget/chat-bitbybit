﻿namespace Messanger.Core.Entities;

public class Bot : Interlocutor
{
    public Guid CreatorId { get; set; }
    
    public User Creator { get; set; }

    public ICollection<Step> Steps { get; set; }
}