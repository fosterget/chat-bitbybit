﻿using Messanger.Core.Enums;

namespace Messanger.Core.Entities;

public class File
{
    public Guid Id { get; set; }
    
    public string Name { get; set; }
    
    public string ContentType { get; set; }
    
    public string? Url { get; set; }
    
    public Message Message { get; set; }
    
    public FileType Type { get; set; }
}