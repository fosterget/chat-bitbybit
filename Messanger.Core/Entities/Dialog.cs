﻿using Messanger.Core.Enums;

namespace Messanger.Core.Entities
{
    public class Dialog
    {
        public Guid Id { get; set; }

        public string? LastMessageText { get; set; }

        public DialogType Type { get; set; }

        public DateTime Updated { get; set; }

        public Team? Team { get; set; }

        public ICollection<Message> Messages { get; set; }

        public ICollection<Interlocutor> Interlocutors { get; set; }
    }
}