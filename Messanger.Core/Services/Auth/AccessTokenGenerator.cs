﻿using Messanger.Core.Interfaces.Services.Auth;
using Messanger.Core.Models.Auth;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Messanger.Core.Constants;

namespace Messanger.Core.Services.Auth
{
    public class AccessTokenGenerator : IAccessTokenGenerator
    {
        private readonly JwtSettings _jwtSettings;

        public AccessTokenGenerator(IOptions<JwtSettings> jwtSettings)
        {
            _jwtSettings = jwtSettings.Value;
        }

        public string GenerateToken(Guid id)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtSettings.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Expires = DateTime.UtcNow.AddDays(AuthConstants.AccessTokenLifetimeDays),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                Claims = new Dictionary<string, object>()
                {
                    { AuthConstants.Claims.Id, id.ToString() }, 
                    { ClaimTypes.NameIdentifier, id.ToString() }
                }
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
