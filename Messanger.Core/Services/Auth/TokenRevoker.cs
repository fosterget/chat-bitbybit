﻿using Messanger.Core.Exceptions;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Repositories;
using Messanger.Core.Interfaces.Services.Auth;

namespace Messanger.Core.Services.Auth
{
    public class TokenRevoker : ITokenRevoker
    {
        private readonly IUnitOfWork _unitOfWork;

        public TokenRevoker(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task RevokeTokenAsync(string token, string ipAddress)
        {
            var user = await _unitOfWork.Users.GetUserByRefreshTokenAsync(token);
            var refreshToken = user.RefreshTokens.First(t => t.Token == token);

            if (!refreshToken.IsActive)
                throw new AuthException("Token is invalid");

            // Revoke
            refreshToken.Revoked = DateTime.UtcNow;
            refreshToken.RevokedByIp = ipAddress;
            refreshToken.ReasonRevoked = "Revoked without replacement";

            _unitOfWork.Users.Update(user);
            await _unitOfWork.Complete();
        }
    }
}
