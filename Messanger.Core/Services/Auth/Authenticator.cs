﻿using Messanger.Core.Entities;
using Messanger.Core.Exceptions;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Services.Auth;
using Messanger.Core.Models.Auth;
using Microsoft.Extensions.Options;

namespace Messanger.Core.Services.Auth
{
    public class Authenticator : IAuthenticator
    {
        private readonly IUnitOfWork _unitOfWork;
        
        private readonly IAccessTokenGenerator _accessTokenGenerator;
        
        private readonly IRefreshTokenGenerator _refreshTokenGenerator;

        private readonly JwtSettings _jwtSettings;

        public Authenticator(
            IUnitOfWork unitOfWork, 
            IAccessTokenGenerator accessTokenGenerator,  
            IRefreshTokenGenerator refreshTokenGenerator,
            IOptions<JwtSettings> jwtSettings)
        {
            _unitOfWork = unitOfWork;
            _accessTokenGenerator = accessTokenGenerator;
            _refreshTokenGenerator = refreshTokenGenerator;
            _jwtSettings = jwtSettings.Value;
        }

        public async Task<AuthResult> AuthenticateAsync(AuthInfo authInfo)
        {
            var user = await _unitOfWork.Users.FindByEmailAsync(authInfo.Email);
            
            ValidateCredentials(user, authInfo.Password);

            var accessToken = _accessTokenGenerator.GenerateToken(user.Id);
            var refreshToken = await _refreshTokenGenerator.GenerateTokenAsync(authInfo.IpAddress);
            user.RefreshTokens.Add(refreshToken);

            RemoveOldRefreshTokens(user);

            _unitOfWork.Users.Update(user);
            await _unitOfWork.Complete();

            return new AuthResult
            {
                Id = user.Id,
                Name = user.Name,
                Surname = user.Surname,
                Email = user.Email,
                ProfileImageUpdated = user.ProfileImageUpdated,
                AccessToken = accessToken,
                RefreshToken = refreshToken.Token
            };
        }

        private static void ValidateCredentials(User? user, string password)
        {
            if (user is null || !BCrypt.Net.BCrypt.Verify(password, user.Password))
            {
                throw new AuthException("Invalid credentials");
            }
        }
        
        private void RemoveOldRefreshTokens(User user)
        {
            user.RefreshTokens.ToList().RemoveAll(t => 
                !t.IsActive &&
                t.Created.AddDays(_jwtSettings.RefreshTokenTTL) <= DateTime.UtcNow);
        }
    }
}
