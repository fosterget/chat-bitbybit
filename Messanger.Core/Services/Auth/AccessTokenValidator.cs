﻿using Messanger.Core.Constants;
using Messanger.Core.Interfaces.Services.Auth;
using Messanger.Core.Models.Auth;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace Messanger.Core.Services.Auth
{
    public class AccessTokenValidator : IAccessTokenValidator
    {
        private readonly JwtSettings _jwtSettings;

        public AccessTokenValidator(IOptions<JwtSettings> jwtSettings)
        {
            _jwtSettings = jwtSettings.Value;
        }

        public void ApplyValidationOptions(JwtBearerOptions options)
        {
            var key = Encoding.ASCII.GetBytes(_jwtSettings.Secret);
            options.RequireHttpsMetadata = false;
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = true,
            };
        }
    }
}
