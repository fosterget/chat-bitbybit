﻿using Messanger.Core.Entities;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Repositories;
using Messanger.Core.Interfaces.Services.Auth;
using Messanger.Core.Models.Auth;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Cryptography;
using System.Text;
using Messanger.Core.Constants;

namespace Messanger.Core.Services.Auth
{
    public class RefreshTokenGenerator : IRefreshTokenGenerator
    {
        private readonly IUnitOfWork _unitOfWork;

        public RefreshTokenGenerator(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<RefreshToken> GenerateTokenAsync(string ipAddress)
        {
            var refreshToken = new RefreshToken
            {
                Token = await GetUniqueToken(),
                Expires = DateTime.UtcNow.AddDays(AuthConstants.RefreshTokenLifetimeDays),
                Created = DateTime.UtcNow,
                CreatedByIp = ipAddress
            };

            return refreshToken;
        }

        private async Task<string> GetUniqueToken()
        {
            var token = Convert.ToBase64String(RandomNumberGenerator.GetBytes(64));

            var isTokenUnique = await _unitOfWork.Users.IsTokenUniqueAsync(token);

            if(!isTokenUnique)
                return await GetUniqueToken();

            return token;
        }
    }
}
