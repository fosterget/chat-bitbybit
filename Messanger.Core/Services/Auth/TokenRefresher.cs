﻿using Messanger.Core.Entities;
using Messanger.Core.Interfaces.Repositories;
using Messanger.Core.Interfaces.Services.Auth;
using Messanger.Core.Models.Auth;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messanger.Core.Exceptions;
using Messanger.Core.Interfaces;

namespace Messanger.Core.Services.Auth
{
    public class TokenRefresher : ITokenRefresher
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IRefreshTokenGenerator _refreshTokenGenerator;

        private readonly IAccessTokenGenerator _accessTokenGenerator;

        private readonly JwtSettings _jwtSettings;

        public TokenRefresher(
            IUnitOfWork unitOfWork,
            IRefreshTokenGenerator refreshTokenGenerator,
            IAccessTokenGenerator accessTokenGenerator,
            IOptions<JwtSettings> jwtSettings)
        {
            _unitOfWork = unitOfWork;
            _refreshTokenGenerator = refreshTokenGenerator;
            _accessTokenGenerator = accessTokenGenerator;
            _jwtSettings = jwtSettings.Value;
        }

        public async Task<AuthResult> RefreshTokenAsync(string token, string ipAddress)
        {
            var user = await _unitOfWork.Users.GetUserByRefreshTokenAsync(token);
            var refreshToken = user.RefreshTokens.First(t => t.Token == token);

            // Revoke all descendant because this token has been compromised
            if (refreshToken.IsRevoked)
            {
                RevokeDescendantTokens(refreshToken, user, ipAddress, $"Attempted reuse of revoked ancestor token: {token}");

                _unitOfWork.Users.Update(user);
                await _unitOfWork.Complete();

                throw new InvalidArgumentException("Attempted reuse of revoked token");
            }

            if (!refreshToken.IsActive)
                throw new InvalidArgumentException("Invalid token");

            // Rotate refresh token
            var newRefreshToken = await RotateTokenAsync(refreshToken, ipAddress);
            user.RefreshTokens.Add(newRefreshToken);

            // Remove user`s old refresh tokens
            RemoveOldRefreshTokens(user);

            _unitOfWork.Users.Update(user);
            await _unitOfWork.Complete();

            var accessToken = _accessTokenGenerator.GenerateToken(user.Id);

            return new AuthResult
            {
                Id = user.Id,
                Name = user.Name,
                Surname = user.Surname,
                Email = user.Email,
                AccessToken = accessToken,
                RefreshToken = newRefreshToken.Token
            };
        }

        /// <summary>
        /// Method revokes old token and generate a new one
        /// </summary>
        private async Task<RefreshToken> RotateTokenAsync(RefreshToken token, string ipAddress)
        {
            var newRefreshToken = await _refreshTokenGenerator.GenerateTokenAsync(ipAddress);
            RevokeToken(token, ipAddress, "Replace by new token", newRefreshToken.Token);
            return newRefreshToken;
        }
        
        /// <summary>
        /// Revoke all refresh tokens and their inheritors (by ReplacedByToken property)
        /// </summary>
        private void RevokeDescendantTokens(RefreshToken token, User user, string ipAddress, string reason)
        {
            if (!string.IsNullOrEmpty(token.ReplacedByToken))
            {
                //var childToken = token.ReplacedByToken;
                var childToken = user.RefreshTokens.Single(t => t.Token == token.ReplacedByToken);

                if (childToken.IsActive)
                    RevokeToken(childToken, ipAddress, reason);
                else
                    RevokeDescendantTokens(childToken, user, ipAddress, reason);
            }
        }

        private void RevokeToken(RefreshToken token, string ipAddress, string reason, string? replacedByToken = null)
        {
            token.Revoked = DateTime.UtcNow;
            token.RevokedByIp = ipAddress;
            token.ReasonRevoked = reason;
            token.ReplacedByToken = replacedByToken;
        }

        private void RemoveOldRefreshTokens(User user)
        {
            user.RefreshTokens.ToList().RemoveAll(t =>
                !t.IsActive &&
                t.Created.AddDays(_jwtSettings.RefreshTokenTTL) <= DateTime.UtcNow);
        }
    }
}
