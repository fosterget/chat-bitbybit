﻿using Messanger.Core.Entities;
using Messanger.Core.Exceptions;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Services.Chat;
using Messanger.Core.Models.Chat;

namespace Messanger.Core.Services.Chat;

public class MessageFinder : IMessageFinder
{
    private readonly IUnitOfWork _unitOfWork;

    public MessageFinder(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<IEnumerable<Message>> GetMessagesAsync(Guid userId, Guid dialogId, PaginationInfo pagination)
    {
        await ValidateDialogId(userId, dialogId);

        var messages = await _unitOfWork.Messages.GetByDialogId(dialogId, pagination.Offset, pagination.Limit);

        return messages;
    }
    
    private async Task ValidateDialogId(Guid userId, Guid dialogId)
    {
        var user = await _unitOfWork.Users.GetUserWithDialogsByIdAsync(userId)
            ?? throw new InvalidArgumentException("User with such id does not exist");

        if(user.Dialogs.All(dialog => dialog.Id != dialogId))
        {
            throw new InvalidArgumentException("User doesn't have access to this dialog");
        }
    }
}