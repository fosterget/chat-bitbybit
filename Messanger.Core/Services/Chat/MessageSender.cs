﻿using Messanger.Core.Entities;
using Messanger.Core.Enums;
using Messanger.Core.Exceptions;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Repositories.BlobStorage;
using Messanger.Core.Interfaces.Services.Chat;
using Messanger.Core.Models.Chat;
using Microsoft.AspNetCore.Http;
using File = Messanger.Core.Entities.File;

namespace Messanger.Core.Services.Chat;

public class MessageSender : IMessageSender
{
    private readonly IUnitOfWork _unitOfWork;

    private readonly IDialogCreator _dialogCreator;

    private readonly IDialogImageRepository _dialogImageRepository;

    public MessageSender(IUnitOfWork unitOfWork, IDialogCreator dialogCreator, IDialogImageRepository dialogImageRepository)
    {
        _unitOfWork = unitOfWork;
        _dialogCreator = dialogCreator;
        _dialogImageRepository = dialogImageRepository;
    }

    public async Task<Message> SendMessageByDialogAsync(MessageInfo messageInfo)
    {
        var dialog = await GetDialog(messageInfo.DialogId, messageInfo.AuthorId) 
                     ?? throw new InvalidArgumentException("Dialog with such id does not exist");

        return await CreateMessage(dialog, messageInfo);
    }

    public async Task<Message> SendMessageByRecipientAsync(MessageInfo messageInfo)
    {
        var authorDialogs = await _unitOfWork.Dialogs.GetByUserId(messageInfo.AuthorId);
        var dialog = authorDialogs.FirstOrDefault(d => d.Interlocutors.Any(i => i.Id == messageInfo.RecipientId));
        
        if (dialog is null)
        {
            dialog = await _dialogCreator.CreateDialogAsync(new List<Guid>()
            {
                messageInfo.AuthorId,
                messageInfo.RecipientId
            });   
        }

        return await CreateMessage(dialog, messageInfo);
    }

    public async Task<Message> SendImage(Guid userId, Guid dialogId, IFormFile file)
    {
        // throw new NotImplementedException();
        ValidateImageFile(file);
        
        var dialog = await GetDialog(dialogId, userId);
        var messageInfo = new MessageInfo
        {
            AuthorId = userId,
            DialogId = dialogId,
            File = file,
            FileType = FileType.Image,
            Text = "🖼"
        };
        var message = await CreateMessage(dialog, messageInfo);
        
        var imageUrl = await _dialogImageRepository.AddFile(file, message.FileId.ToString()!);
        message.File.Url = imageUrl;
        _unitOfWork.Files.Update(message.File);
        await _unitOfWork.Complete();
        
        return message;
    }

    private async Task<Dialog> GetDialog(Guid dialogId, Guid userId)
    {
        var dialog = await _unitOfWork.Dialogs.GetWithMessagesAndUsers(dialogId) 
                     ?? throw new InvalidArgumentException("Dialog with such id does not exist");

        if (dialog.Interlocutors.All(u => u.Id != userId))
            throw new InvalidArgumentException("User does not have access to this dialog");
            
        return dialog;
    }

    private async Task<Message> CreateMessage(Dialog dialog, MessageInfo messageInfo)
    {
        var message = new Message()
        {
            Date = DateTime.UtcNow,
            Text = messageInfo.Text,
            AuthorId = messageInfo.AuthorId,
            Dialog = dialog
        };
        
        if (messageInfo.ButtonTitles?.Count > 0)
        {
            message.Buttons = messageInfo.ButtonTitles.Select(title => new MessageButton
            {
                Title = title
            }).ToList();
        }

        if (messageInfo.File is not null && messageInfo.FileType is not null)
        {
            message.File = new File()
            {
                ContentType = messageInfo.File.ContentType,
                Name = messageInfo.File.FileName,
                Type = (FileType)messageInfo.FileType
            };
        }
        
        dialog.LastMessageText = message.Text;
        dialog.Updated = message.Date;
        _unitOfWork.Messages.Add(message);

        await _unitOfWork.Complete();
        return message;
    }

    private static void ValidateImageFile(IFormFile file)
    {
        if (file.Length < 1 || !file.ContentType.Contains("image") || Path.HasExtension(file.Name))
            throw new InvalidArgumentException("You file is not an image or does not have extension");
    }
}