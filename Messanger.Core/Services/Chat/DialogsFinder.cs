﻿using Messanger.Core.Entities;
using Messanger.Core.Enums;
using Messanger.Core.Exceptions;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Repositories;
using Messanger.Core.Interfaces.Services.Chat;

namespace Messanger.Core.Services.Chat
{
    public class DialogsFinder : IDialogsFinder
    {
        private readonly IUnitOfWork _unitOfWork;

        public DialogsFinder(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Dialog>> GetPersonalDialogsAsync(Guid userId, DialogType type)
        {
            var user = await _unitOfWork.Users.GetByIdAsync(userId);
            if(user is null)
            {
                throw new AuthException("Invalid token");
            }
            var dialogs = await _unitOfWork.Dialogs.GetByUser(user, type);

            return dialogs;
        }
    }
}
