﻿using Messanger.Core.Entities;
using Messanger.Core.Enums;
using Messanger.Core.Exceptions;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Services.Chat;

namespace Messanger.Core.Services.Chat;

public class DialogCreator : IDialogCreator
{
    private readonly IUnitOfWork _unitOfWork;

    public DialogCreator(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    
    /// <inheritdoc />>
    public async Task<Dialog> CreateDialogAsync(IEnumerable<Guid> userIds, DialogType? dialogType = null)
    {
        var dialog = new Dialog
        {
            Interlocutors = new List<Interlocutor>(),
            Messages = new List<Message>()
        };
        foreach(var id in userIds)
        {
            var user = await _unitOfWork.Interlocutors.GetByIdAsync(id);
            if (user is null)
                throw new InvalidArgumentException("User with such id does not exist");
            
            dialog.Interlocutors.Add(user);
        }

        if (dialogType is not null)
        {
            dialog.Type = (DialogType)dialogType;
        } 
        else if (dialog.Interlocutors.Any(interlocutor => interlocutor.Type is InterlocutorType.Bot))
        {
            dialog.Type = DialogType.Bot;
        } 
        else if (dialog.Interlocutors.Count > 2)
        {
            dialog.Type = DialogType.Group;
        }
        else
        {
            dialog.Type = DialogType.Personal;
        } 
        
        _unitOfWork.Dialogs.Add(dialog);

        return dialog;
    }
}