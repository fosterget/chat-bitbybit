﻿using Messanger.Core.Entities;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Services.Team;

namespace Messanger.Core.Services.Team;

public class TeamsFinder : ITeamsFinder
{
    private readonly IUnitOfWork _unitOfWork;

    public TeamsFinder(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    
    public async Task<IEnumerable<Entities.Team>> GetTeamsCreatedByUserAsync(Guid userId)
    {
        return await _unitOfWork.Teams.GetTeamsByOwnerAsync(userId);
    }

    public async Task<IEnumerable<User>> GetTeamUsersAsync(Guid teamId)
    {
        return await _unitOfWork.Teams.GetTeamUsers(teamId);
    }
}