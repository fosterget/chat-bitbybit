using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Services.Team;

namespace Messanger.Core.Services.Team;

public class TeamUsersManager : ITeamUsersManager
{
    private readonly IUnitOfWork _unitOfWork;

    public TeamUsersManager(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    
    public async Task AddUser(Guid teamId, Guid userId)
    {
        var team = await _unitOfWork.Teams.GetTeamWithUsersAsync(teamId);
        if (team.Users.All(t => t.Id != userId))
        {
            var user = await _unitOfWork.Users.GetByIdAsync(userId);
            team.Users.Add(user);
            await _unitOfWork.Complete();
        }
    }

    public async Task DeleteUser(Guid teamId, Guid userId)
    {
        var team = await _unitOfWork.Teams.GetTeamWithUsersAsync(teamId);
        if (team.Users.Any(t => t.Id == userId))
        {
            var user = await _unitOfWork.Users.GetByIdAsync(userId);
            team.Users.Remove(user);
            await _unitOfWork.Complete();
        }
    }
}