using Messanger.Core.Entities;
using Messanger.Core.Enums;
using Messanger.Core.Exceptions;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Services.Chat;
using Messanger.Core.Interfaces.Services.Team;

namespace Messanger.Core.Services.Team;

public class TeamCreator : ITeamCreator
{
    private readonly IUnitOfWork _unitOfWork;

    private readonly IDialogCreator _dialogCreator;

    public TeamCreator(IUnitOfWork unitOfWork, IDialogCreator dialogCreator)
    {
        _unitOfWork = unitOfWork;
        _dialogCreator = dialogCreator;
    }
    
    public async Task<Entities.Team> Create(Guid creatorId, string name, List<string> userIds)
    {
        var userGuids = userIds
            .Distinct()
            .Select(id => new Guid(id))
            .ToList();
        
        if (!userGuids.Contains(creatorId))
            userGuids.Add(creatorId);
        
        var teamDialog = await _dialogCreator.CreateDialogAsync(userGuids, DialogType.Group);
        var teamParticipants = new List<User>();
        
        foreach (var userId in userGuids)
        {
            var user = await _unitOfWork.Users.GetByIdAsync(userId);
            if (user is null)
                throw new InvalidArgumentException("User with such id does not exist");
            
            teamParticipants.Add(user);
        }

        var team = new Entities.Team
        {
            Name = name,
            Dialog = teamDialog,
            Users = teamParticipants,
            OwnerId = creatorId
        };
        
        _unitOfWork.Teams.Add(team);
        await _unitOfWork.Complete();

        return team;
    }
}