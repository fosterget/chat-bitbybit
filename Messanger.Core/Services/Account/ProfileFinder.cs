﻿using Messanger.Core.Entities;
using Messanger.Core.Exceptions;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Repositories.BlobStorage;
using Messanger.Core.Interfaces.Services.Account;
using Messanger.Core.Models.Common;
using FileNotFoundException = Messanger.Core.Exceptions.FileNotFoundException;

namespace Messanger.Core.Services.Account;

public class ProfileFinder : IProfileFinder
{
    private readonly IUnitOfWork _unitOfWork;

    private readonly IProfileImageRepository _profileImageRepository;

    public ProfileFinder(IUnitOfWork unitOfWork, IProfileImageRepository profileImageRepository)
    {
        _unitOfWork = unitOfWork;
        _profileImageRepository = profileImageRepository;
    }

    public async Task<User> FindById(Guid userId)
    {
        return await _unitOfWork.Users.GetByIdAsync(userId)
               ?? throw new AuthException("User with such id does not exist");
    }

    public async Task<IEnumerable<User>> FindByName(string query, Guid userId)
    {
        var users = await _unitOfWork.Users.FindByNameAndSurname(query, userId);
        return users;
    }

    public async Task<FileDTO> GetUserProfileImage(Guid userId)
    {
        var user = await _unitOfWork.Users.GetByIdAsync(userId)
            ?? throw new InvalidArgumentException("User with such id does not exist");
        
        // if (user.ProfileImageUpdated == default)
        //     throw new FileNotFoundException("Profile image");

        FileDTO file;
        try
        {
            file = await _profileImageRepository.GetFile(userId.ToString());
        }
        catch
        {
            throw new FileNotFoundException("Profile image");
        }
        return file;
    }
}