﻿using Messanger.Core.Entities;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Services.Account;

namespace Messanger.Core.Services.Account;

public class ContactsFinder : IContactsFinder
{
    private readonly IUnitOfWork _unitOfWork;

    public ContactsFinder(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    
    public async Task<IEnumerable<User>> GetByUserId(Guid userId)
    {
        var contacts = await _unitOfWork.Users.GetUserContacts(userId);
        return contacts;
    }
}