﻿using Messanger.Core.Entities;
using Messanger.Core.Exceptions;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Repositories;
using Messanger.Core.Interfaces.Services.Account;
using Messanger.Core.Models.Account;

namespace Messanger.Core.Services.Account
{
    public class Registrar : IRegistrar
    {
        private readonly IUnitOfWork _unitOfWork;

        public Registrar(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task RegisterUser(RegisterInfo registerInfo)
        {
            await ValidateRegisterInfo(registerInfo);

            var passwordHash = BCrypt.Net.BCrypt.HashPassword(registerInfo.Password);
            
            var user = new User()
            {
                Name = registerInfo.Name,
                Surname = registerInfo.Surname,
                Email = registerInfo.Email,
                Password = passwordHash,
                CreationDate = DateTime.UtcNow
            };

            _unitOfWork.Users.Add(user);
            await _unitOfWork.Complete();
        }

        private async Task ValidateRegisterInfo(RegisterInfo info)
        {
            if(await _unitOfWork.Users.GetWhereSingleAsync(u => u.Email == info.Email) is not null)
            {
                throw new AlreadyExistsException("User with this email");
            }
        }
    }
}
