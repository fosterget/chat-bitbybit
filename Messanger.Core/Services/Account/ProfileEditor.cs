﻿using System.Net;
using System.IO;
using System.Drawing;
using System.Net.Mime;
using Messanger.Core.Entities;
using Messanger.Core.Exceptions;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Repositories.BlobStorage;
using Messanger.Core.Interfaces.Services.Account;
using Messanger.Core.Models.Account;
using Microsoft.AspNetCore.Http;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;

namespace Messanger.Core.Services.Account;

public class ProfileEditor : IProfileEditor
{
    private readonly IUnitOfWork _unitOfWork;

    private readonly IProfileImageRepository _profileImageRepository;

    public ProfileEditor(IUnitOfWork unitOfWork, IProfileImageRepository profileImageRepository)
    {
        _unitOfWork = unitOfWork;
        _profileImageRepository = profileImageRepository;
    }
    
    public async Task<User> Edit(ProfileInfo profileInfo)
    {
        var user = await _unitOfWork.Users.GetByIdAsync(profileInfo.UserId)
            ?? throw new InvalidArgumentException("User with such id does not exist");

        user.Name = profileInfo.Name;
        user.Surname = profileInfo.Surname;
        user.Bio = profileInfo.Bio;
        user.Position = profileInfo.Position;
        if (profileInfo.Email != user.Email)
        {
            var userWithEmail = await _unitOfWork.Users.FindByEmailAsync(profileInfo.Email);
            if (userWithEmail is not null)
            {
                throw new AlreadyExistsException("User with such email");
            }
            
            user.Email = profileInfo.Email;
        }

        _unitOfWork.Users.Update(user);
        await _unitOfWork.Complete();

        return user;
    }

    public async Task<User> ChangeImage(IFormFile file, Guid userId)
    {
        var user = await _unitOfWork.Users.GetByIdAsync(userId)
            ?? throw new InvalidArgumentException("User with such id does not exist");
        
        ValidateImageFile(file);
        
        var imageUrl = await _profileImageRepository.AddFile(file, user.Id.ToString() + DateTime.UtcNow);
        user.ProfileImageUpdated = DateTime.UtcNow;
        user.ProfileImageUrl = imageUrl;
        
        _unitOfWork.Users.Update(user);
        await _unitOfWork.Complete();

        return user;
    }

    private static void ValidateImageFile(IFormFile file)
    {
        if (file.Length < 1 || !file.ContentType.Contains("image") || Path.HasExtension(file.Name))
            throw new InvalidArgumentException("You file is not an image or does not have extension");
        
        // using var imageReadStream = new MemoryStream();
        // file.CopyTo(imageReadStream);
        // try
        // {
        //     Image.Load(imageReadStream);
        // }
        // catch
        // {
        //     throw new InvalidArgumentException("You file is not an image");
        // }
    }
}