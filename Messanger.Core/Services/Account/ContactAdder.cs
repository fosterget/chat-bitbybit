﻿using Messanger.Core.Entities;
using Messanger.Core.Exceptions;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Services.Account;
using Messanger.Core.Interfaces.Services.Chat;
using Messanger.Core.Models.Chat;

namespace Messanger.Core.Services.Account;

public class ContactAdder : IContactAdder
{
    private readonly IUnitOfWork _unitOfWork;

    private readonly IMessageSender _messageSender;

    public ContactAdder(IUnitOfWork unitOfWork, IMessageSender messageSender)
    {
        _unitOfWork = unitOfWork;
        _messageSender = messageSender;
    }
    
    public async Task<User> Add(Guid userId, Guid contactId)
    {
        if (userId == contactId)
            throw new InvalidArgumentException("Contact id is equal to current user id");
        
        var user = await _unitOfWork.Users.GetUserWithContacts(userId) 
                   ?? throw new InvalidArgumentException("User with such id does not exits");
        var contact = await _unitOfWork.Users.GetByIdAsync(contactId) 
                      ?? throw new InvalidArgumentException("User with such id does not exits");

        if (user.Contacts.Any(u => u.Id == contactId))
            throw new ArgumentException("User is already added in contacts");
        
        user.Contacts.Add(contact);
        _unitOfWork.Users.Update(user);
        await _messageSender.SendMessageByRecipientAsync(new MessageInfo
        {
            AuthorId = userId,
            RecipientId = contactId,
            Text = $"Hello, {contact.Name}!"
        });
        
        return user;
    }
}