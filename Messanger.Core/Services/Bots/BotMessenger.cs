﻿using Messanger.Core.Entities;
using Messanger.Core.Enums;
using Messanger.Core.Exceptions;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Repositories;
using Messanger.Core.Interfaces.Services.Bots;
using Messanger.Core.Interfaces.Services.Chat;
using Messanger.Core.Models.Chat;

namespace Messanger.Core.Services.Bots;

public class BotMessenger : IBotMessenger
{
    private readonly IUnitOfWork _unitOfWork;

    private readonly IMessageSender _messageSender;
    
    private readonly IBotDataRepository _botDataRepository;

    public BotMessenger(IUnitOfWork unitOfWork, IMessageSender messageSender, IBotDataRepository botDataRepository)
    {
        _unitOfWork = unitOfWork;
        _messageSender = messageSender;
        _botDataRepository = botDataRepository;
    }
    
    public async Task<List<Message>> SendMessageAsync(Guid userId, Guid botId, string messageText)
    {
        var userMessage = await _messageSender.SendMessageByRecipientAsync(new MessageInfo
        {
            AuthorId = userId,
            RecipientId = botId,
            Text = messageText
        });

        var userData = await _botDataRepository.GetUserDataAsync(botId, userId);

        Step? step, nextStep = default;
        if (userData.ContainsKey("step") && messageText != "/start")
        {
            step = await _unitOfWork.Steps.GetByIdWithSteps(new Guid(userData["step"]))
                ?? throw new InvalidArgumentException("Wrong bot id from botDataRepository");
        }
        else
        {
            step = await _unitOfWork.Steps.GetByBotIdWithSteps(botId)
                   ?? throw new InvalidArgumentException("Wrong bot id");
            userData["step"] = step.Id.ToString();
        }

        if (messageText != "/start")
        {
            if (step.AnswerType is AnswerType.Text)
            {
                if (step.SaveData)
                    userData[step.Name] = messageText;
                
                if (step.NextStep is not null)
                {
                    userData["step"] = step.NextStep.Id.ToString();
                    nextStep = step.NextStep;
                }
            } else if (step.AnswerType is AnswerType.Button)
            {
                var button = step.Buttons!.FirstOrDefault(b => b.Title == messageText);

                if (button is null)
                {
                    // TODO: SEND MESSAGE WRONG BUTTON
                }
                else
                {
                    if (step.SaveData)
                    {
                        userData[step.Name] = button.Title;
                    }

                    if (button.NextStepId is not null)
                    {
                        userData["step"] = button.NextStep.Id.ToString();
                        nextStep = button.NextStep;
                    }
                }
            }
        }
        else
        {
            nextStep = step;
        }

        await _botDataRepository.SaveUserDataAsync(botId, userId, userData);
        var resultMessages = new List<Message>{ userMessage };

        if (nextStep is not null)
        {
            nextStep = await _unitOfWork.Steps.GetByIdWithSteps(nextStep.Id);
            var botAnswer = await _messageSender.SendMessageByRecipientAsync(new MessageInfo
            {
                AuthorId = botId,
                RecipientId = userId,
                Text = nextStep?.Text ?? "",
                ButtonTitles = nextStep?.Buttons?.Select(b => b.Title).ToList()
            });
            resultMessages.Add(botAnswer);
        }

        return resultMessages;
    }
}