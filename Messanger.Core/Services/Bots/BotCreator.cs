﻿using Messanger.Core.Entities;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Repositories;
using Messanger.Core.Interfaces.Services.Bots;
using Messanger.Core.Models.Bots;

namespace Messanger.Core.Services.Bots;

public class BotCreator : IBotCreator
{
    private readonly IUnitOfWork _unitOfWork;

    private readonly IBotDataRepository _botDataRepository;
    
    public BotCreator(IUnitOfWork unitOfWork, IBotDataRepository botDataRepository)
    {
        _unitOfWork = unitOfWork;
        _botDataRepository = botDataRepository;
    }
    
    public async Task Create(Guid creatorId, string botName, List<StepInfo> stepsInfo)
    {
        var bot = new Bot
        {
            CreatorId = creatorId,
            Name = botName,
            CreationDate = DateTime.UtcNow,
            Steps = new List<Step>()
        };
        
        foreach (var step in stepsInfo)
        {
            var newStep = new Step
            {
                Name = step.Name,
                Text = step.Text,
                SaveData = step.SaveData,
                AnswerType = step.AnswerType,
                Buttons = new List<StepButton>()
            };
            bot.Steps.Add(newStep);
        }
        
        for(var i = 0; i < stepsInfo.Count; i++)
        {
            if (stepsInfo[i].NextStepId is not null)
            {
                bot.Steps.ElementAt(i).NextStep = bot.Steps.ElementAt((int)stepsInfo[i].NextStepId! - 1);
            }
        
            if (stepsInfo[i].Buttons is null || !stepsInfo[i].Buttons.Any()) continue;
            
            foreach (var button in stepsInfo[i].Buttons!)
            {
                var newButton = new StepButton
                {
                    Title = button.Title
                };
        
                if (button.NextStepId is not null)
                    newButton.NextStep = bot.Steps.ElementAt((int)button.NextStepId - 1);
                    
                bot.Steps.ElementAt(i).Buttons!.Add(newButton);
            }
        }
        
        _unitOfWork.Bots.Add(bot);
        await _botDataRepository.CreateBotAsync(bot.Id, creatorId);
        await _unitOfWork.Complete();
        
        // var userEntry = await _botDataRepository.GetUserData(bot.Id, creatorId);
        //
        // var userData = userEntry ?? new Dictionary<string, string>();
        //
        // userData["step"] = "Very beginning";
        // await _botDataRepository.SaveUserData(bot.Id, creatorId, userData);
    }
}