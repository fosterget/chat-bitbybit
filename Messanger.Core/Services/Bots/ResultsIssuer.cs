﻿using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Services;
using Messanger.Core.Models.Bots;

namespace Messanger.Core.Services.Bots;

public class ResultsIssuer : IResultsIssuer
{
    private readonly IBotResultsCsvGenerator _botResultsCsvGenerator;

    public ResultsIssuer(IBotResultsCsvGenerator botResultsCsvGenerator)
    {
        _botResultsCsvGenerator = botResultsCsvGenerator;
    }
    
    public async Task<BotResults> GetResults(Guid botId)
    {
        return await _botResultsCsvGenerator.GetResults(botId);
    }
}