﻿using Messanger.Core.Entities;
using Messanger.Core.Exceptions;
using Messanger.Core.Interfaces;
using Messanger.Core.Interfaces.Services.Bots;

namespace Messanger.Core.Services.Bots;

public class BotFinder : IBotFinder
{
    private readonly IUnitOfWork _unitOfWork;

    public BotFinder(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    
    public async Task<Bot> FindById(Guid id)
    {
        return await _unitOfWork.Bots.GetBotByIdAsync(id)
                  ?? throw new InvalidArgumentException("Bot id is invalid");
    }

    public async Task<IEnumerable<Bot>> FindByCreator(Guid id)
    {
        return await _unitOfWork.Bots.GetWhereAsync(bot => bot.CreatorId == id)
                  ?? throw new InvalidArgumentException("Creator id is invalid");;
    }

    public async Task<IEnumerable<Bot>> FindByName(string name)
    {
        return await _unitOfWork.Bots.GetWhereAsync(bot => bot.Name.ToLower().Contains(name.ToLower()));
    }
}