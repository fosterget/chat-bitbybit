using System.Net;

namespace Messanger.Core.Exceptions;

public class MissingArgumentException : Exception
{
    public const int StatusCode = (int) HttpStatusCode.BadRequest;
    private const string Name = nameof(MissingArgumentException);

    public MissingArgumentException(string message)
        : base($"{Name}: {message} is missing")
    {
    }
}