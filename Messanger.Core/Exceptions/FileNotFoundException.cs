using System.Net;

namespace Messanger.Core.Exceptions;

public class FileNotFoundException : Exception
{
    public const int StatusCode = (int) HttpStatusCode.NotFound;
    private const string Name = nameof(FileNotFoundException);

    public FileNotFoundException(string message)
        : base($"{Name}: {message} file does not exist.")
    {
    }
}