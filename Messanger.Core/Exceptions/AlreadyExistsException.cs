using System.Net;

namespace Messanger.Core.Exceptions;

public class AlreadyExistsException : Exception
{
    public const int StatusCode = (int) HttpStatusCode.Conflict;
    private const string Name = nameof(AlreadyExistsException);

    public AlreadyExistsException(string message)
        : base($"{Name}: {message} already exists")
    {
    }
}