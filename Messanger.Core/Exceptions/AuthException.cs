using System.Net;

namespace Messanger.Core.Exceptions;

public class AuthException : Exception
{
    public const int StatusCode = (int) HttpStatusCode.Unauthorized;
    private const string Name = nameof(AuthException);

    public AuthException(string message)
        : base($"{Name}: {message}")
    {
    }
}