using System.Net;

namespace Messanger.Core.Exceptions;

public class InvalidArgumentException : Exception
{
    public const int StatusCode = (int) HttpStatusCode.UnprocessableEntity;
    private const string Name = nameof(InvalidArgumentException);

    public InvalidArgumentException(string message)
        : base($"{Name}: {message}")
    {
    }
}